/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the test suite of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtTest/QtTest>
#include <private/q3dsuipparser_p.h>
#include <private/q3dsutils_p.h>
#include <private/q3dsdatamodelparser_p.h>

class tst_Q3DSVariantParser : public QObject
{
    Q_OBJECT

private slots:
    bool doesSceneContainLayer(Q3DSScene *scene, QString layerName);

    void initTestCase();
    void cleanup();

    void testInvalidVariantsFiltered();
    void testNoVariantsFiltered();

    void testAllVariantFiltered();
    void testCubeOnlyVariant();
    void testSphereOnlyVariant();
    void testConeOnlyVariant();

    void testCenterVariant();
    void testEdgeVariant();
    void testEdgeAndAllVariant();
    void testEdgeAndCubeVariant();

private:
    QString presName = QLatin1String("varianttest");
};

bool tst_Q3DSVariantParser::doesSceneContainLayer(Q3DSScene *scene, QString layerName)
{
    bool layerFound = false;
    for (int i = 0; i < scene->childCount(); ++i) {
        Q3DSGraphObject *n = scene->childAtIndex(i);
        if (n->type() == Q3DSGraphObject::Layer)
            layerFound |= (n->name() == layerName);
    }

    return layerFound;
}

void tst_Q3DSVariantParser::initTestCase()
{
    Q3DSUtils::setDialogsEnabled(false);
}

void tst_Q3DSVariantParser::cleanup()
{
}

void tst_Q3DSVariantParser::testInvalidVariantsFiltered()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("VarGroupDummy");
    QString variant = QLatin1String("VariantDummy");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testNoVariantsFiltered()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testAllVariantFiltered()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Geom");
    QString variant = QLatin1String("All");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testCubeOnlyVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Geom");
    QString variant = QLatin1String("Cube");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testSphereOnlyVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Geom");
    QString variant = QLatin1String("Sphere");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testConeOnlyVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Geom");
    QString variant = QLatin1String("Cone");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testCenterVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Position");
    QString variant = QLatin1String("Center");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testEdgeVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group = QLatin1String("Position");
    QString variant = QLatin1String("Edge");
    QVector<QStringRef> variants;
    variants.append(QStringRef(&variant));
    variantMap[QStringRef(&group)] = variants;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testEdgeAndAllVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QString group1 = QLatin1String("Position");
    QString variant1 = QLatin1String("Edge");
    QString group2 = QLatin1String("Geom");
    QString variant2 = QLatin1String("All");
    QVector<QStringRef> variants1;
    variants1.append(QStringRef(&variant1));
    variantMap[QStringRef(&group1)] = variants1;
    QVector<QStringRef> variants2;
    variants2.append(QStringRef(&variant2));
    variantMap[QStringRef(&group1)] = variants1;
    variantMap[QStringRef(&group2)] = variants2;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

void tst_Q3DSVariantParser::testEdgeAndCubeVariant()
{
    QHash<QStringRef, QVector<QStringRef>> variantMap;
    QStringList variantList1;
    QString group1 = QLatin1String("Position");
    QString variant1 = QLatin1String("Edge");
    QString group2 = QLatin1String("Geom");
    QString variant2 = QLatin1String("Cube");
    QVector<QStringRef> variants1;
    variants1.append(QStringRef(&variant1));
    variantMap[QStringRef(&group1)] = variants1;
    QVector<QStringRef> variants2;
    variants2.append(QStringRef(&variant2));
    variantMap[QStringRef(&group1)] = variants1;
    variantMap[QStringRef(&group2)] = variants2;

    Q3DSUipParser parser;
    QScopedPointer<Q3DSUipPresentation> pres(
                parser.parse(QLatin1String(":/data/varianttest.uip"),
                             presName,
                             variantMap));
    QVERIFY(!pres.isNull());
    QVERIFY(!parser.readerErrorString().isEmpty());
    Q3DSScene *scene = pres->scene();
    QVERIFY(scene);
    QVERIFY(scene->childCount() > 0);
    QVERIFY(doesSceneContainLayer(scene, QLatin1String("CubeLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("SphereLayer")));
    QVERIFY(!doesSceneContainLayer(scene, QLatin1String("ConeLayer")));
}

#include <tst_q3dsvariantparser.moc>
QTEST_MAIN(tst_Q3DSVariantParser)

TEMPLATE = subdirs

SUBDIRS += \
    uipparser \
    uiaparser \
    meshloader \
    materialparser \
    effectparser \
    uippresentation \
    slidedeck \
    behaviors \
    documents \
    slides \
    slideplayer \
    surfaceviewer \
    q3dslancelot \
    variantparser

qtHaveModule(quick) {
    SUBDIRS += \
        studio3d \
        studio3dengine
}

qtHaveModule(widgets): SUBDIRS += widget

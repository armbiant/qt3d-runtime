TARGET = tst_q3dsstudio3dengine
CONFIG += testcase

QT += qml quick testlib 3dstudioruntime2-private

SOURCES += tst_studio3dengine.cpp

RESOURCES += studio3dengine.qrc

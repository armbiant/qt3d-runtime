/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSSTUDIO3DVIEW_P_H
#define Q3DSSTUDIO3DVIEW_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QQuickItem>

QT_BEGIN_NAMESPACE

class Q3DSStudio3DItem;
class QSGTexture;
class Q3DSLayer3DSGNode;
class Q3DSStudio3DViewTextureProvider;
class Q3DSLayerNode;

class Q3DSStudio3DView : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QObject *engine READ engine WRITE setEngine NOTIFY engineChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(bool sizeLayerToView READ sizeLayerToView WRITE setSizeLayerToView NOTIFY sizeLayerToViewChanged)

public:
    explicit Q3DSStudio3DView(QQuickItem *parent = 0);
    ~Q3DSStudio3DView();

    QObject *engine() const;
    void setEngine(QObject *e);

    QString source() const;
    void setSource(const QString &s);

    bool sizeLayerToView() const;
    void setSizeLayerToView(bool v);

    Q3DSLayer3DSGNode *node() const { return m_node; }

    bool isTextureProvider() const override;
    QSGTextureProvider *textureProvider() const override;

    void notifyTextureChange(QSGTexture *t, int msaaSampleCount);
    void notifyLayerNodeChange(Q3DSLayerNode *layer3DS);

    void resetToDummy();

signals:
    void engineChanged();
    void sourceChanged();
    void sizeLayerToViewChanged();

private:
    QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *nodeData) override;
    void itemChange(QQuickItem::ItemChange change, const QQuickItem::ItemChangeData &changeData) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void touchEvent(QTouchEvent *event) override;

    Q3DSStudio3DItem *m_engine = nullptr;
    QString m_source;
    bool m_sizeLayerToView = true;
    QSGTexture *m_dummyTexture = nullptr;
    Q3DSLayer3DSGNode *m_node = nullptr;
    mutable Q3DSStudio3DViewTextureProvider *m_textureProvider = nullptr;
    bool m_canProvideTexture = true;
    QSGTexture *m_lastTexture = nullptr;
    Q3DSLayerNode *m_layer3DS = nullptr;
    bool m_resetToDummy = false;
};

QT_END_NAMESPACE

#endif // Q3DSSTUDIO3DVIEW_P_H

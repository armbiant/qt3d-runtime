/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsuippresentation_p.h"
#include "q3dsscenemanager_p.h"
#include "q3dsanimationmanagerng_p.h"
#include "animator/q3dsanimator_p.h"
#include "q3dslogging_p.h"

QT_BEGIN_NAMESPACE

static constexpr qint8 componentSuffixToIndex(qint8 c)
{
    return (c >= 'x' && c <= 'z') ? (c - 'x') : -1;
}

static QVector3D toVector3D(const QVariant &qv)
{
    switch (int(qv.type())) {
    case QVariant::Double:
        Q_FALLTHROUGH();
    case QMetaType::Float:
        return QVector3D{QVector2D{*reinterpret_cast<const float *>(qv.constData()), 0.0f}};
    case QVariant::Vector2D:
        return QVector3D{*reinterpret_cast<const QVector2D *>(qv.constData())};
    case QVariant::Vector3D:
        return *reinterpret_cast<const QVector3D *>(qv.constData());
    case QVariant::Color:
    {
        const auto c = qv.value<QColor>();
        return QVector3D{float(c.redF()), float(c.greenF()), float(c.blueF())};
    }
    default:
        qCWarning(lcAnim, "The unsupported type %s attempted used!", qv.typeName());
        return QVector3D();
    }
};

static void addKeyFramesForTrack(const Q3DSAnimationTrack &track, Q3DSAnimator::AnimationDataList *kfl)
{
    const auto type = track.type();
    const auto &keyFrames = track.keyFrames();
    const auto end = keyFrames.constEnd();
    const auto begin = keyFrames.constBegin();
    auto it = begin;
    Q3DSAnimationData keyFrameData;
    keyFrameData.type = Q3DSAnimationData::DataType::KeyFrame;
    while (it != end) {
        switch (type) {
        case Q3DSAnimationTrack::EaseInOut:
        {
            // c1 (t, v) -> first/right control point (ease in).
            // Easing value (for t) is between 0 and 1, where 0 is the current keyframe's start time
            // and 1 is the next keyframe's start time.
            // c1's value is always the same as the current keyframe's value, as that's the
            // only option we support at the moment.

            // c2 (t, v) -> second/left control point (ease out).
            // Easing value (for t) is between 0 and 1, where 0 is the next keyframe's start time
            // and 1 is the current keyframe's start time.
            // c2's value is always the same as the next keyframe's value, as that's the only
            // option we support at the moment.

            // Get normalized value [0..1]
            const float easeIn = qBound(0.0f, (it->easeIn / 100.0f), 1.0f);
            const float easeOut = qBound(0.0f, (it->easeOut / 100.0f), 1.0f);

            // Next and previous keyframes, if any.
            const auto next = ((it + 1) != end) ? (it + 1) : it;
            const auto previous = (it != begin) ? (it - 1) : it;

            // Adjustment to the easing values, to limit the range of the control points,
            // so we get the same "smooth" easing curves as in Studio 1.0
            static const float adjustment = 1.0f / 3.0f;

            // c1
            float dt = (next->time - it->time);
            const float p1t = qBound(it->time, it->time + (dt * easeIn * adjustment), next->time);
            const float p1v = it->value;

            // c2
            dt = (it->time - previous->time);
            const float p2t = qBound(previous->time, it->time - (dt * easeOut * adjustment), it->time);
            const float p2v = it->value;

            keyFrameData.keyFrameData = {it->time, it->value, {p1t, p1v}, {p2t, p2v}};
            kfl->push_back(keyFrameData);
        }
            break;
        case Q3DSAnimationTrack::Bezier:
            keyFrameData.keyFrameData = {it->time, it->value, {it->c1time, it->c1value / 100.0f}, {it->c2time, it->c2value / 100.0f}};
            kfl->push_back(keyFrameData);
            break;
        default:
            keyFrameData.keyFrameData = {it->time, it->value, {it->time, it->value}, {it->time, it->value}};
            kfl->push_back(keyFrameData);
            break;
        }
        ++it;
    }
}

static void buildAnimationData(const Q3DSSlide::AnimationTrackList &tracks,
                               Q3DSAnimator::AnimationDataList *animationDataList)
{
    auto cit = tracks.cbegin();
    const auto cend = tracks.cend();
    while (cit != cend) {
        const auto &track = *cit;
        const auto prop = track.property().split('.');
        const auto comp = (prop.size() > 1) ? Q3DSAnimationData::Component(componentSuffixToIndex(*prop.at(1).toLatin1()))
                                            : Q3DSAnimationData::Component::X;

        // Structure: | Cx | K1..Kn | Cy | K1..Kn | Cz | K1..Kn | T | ... |
        // 1. Create component data
        Q3DSAnimationData component;
        component.type = Q3DSAnimationData::DataType::Component;
        component.componentData.component = comp;
        component.componentData.size = quint16(track.keyFrames().size());
        component.componentData.componentFlags = track.isDynamic() ? Q3DSAnimationData::ComponentFlags::Dynamic
                                                                   : Q3DSAnimationData::ComponentFlags::None;
        animationDataList->push_back(component);

        // 2. Add key frames for component track
        addKeyFramesForTrack(track, animationDataList);

        // 3. Add the target data (if we're at end, the target changes, or the property changes
        if ((cit + 1) == cend || (cit->target() != (cit + 1)->target() || !(cit + 1)->property().startsWith(prop[0]))) {
            Q3DSAnimationData targetData;
            targetData.type = Q3DSAnimationData::DataType::Target;
            targetData.targetData.target = track.target();

            const int pid = Q3DSGraphObject::indexOfProperty(track.target(), prop[0].toLatin1());
            Q_ASSERT(pid != -1);
            targetData.targetData.pid = qint16(pid);
            targetData.targetData.changeFlags = qint8(track.target()->mapChangeFlags({{prop[0]}}));
            // TODO: Find a nice solution for getting the type...
            const auto type = Q3DSGraphObject::readProperty(track.target(), pid).type();
            Q_ASSERT(type != QVariant::Invalid);
            targetData.targetData.type = type;
            animationDataList->push_back(targetData);
        }
        ++cit;
    }
}

void Q3DSAnimationManagerNg::buildSlideAnimation(Q3DSSlide *slide, bool rebuild)
{
    if (!slide)
        return;

    Q3DSSlideAttached *data = slide->attached<Q3DSSlideAttached>();
    Q_ASSERT(data);
    if (!data->animatorNg)
        data->animatorNg = new Q3DSAnimator;

    if (!rebuild && !data->animatorNg->animationDataList.isEmpty())
        return;

    auto &animationDataList = data->animatorNg->animationDataList;
    animationDataList.clear();

    // NOTE: the assumption here is that tracks are ordered as, e.g., rotation.x, rotation.y...
    const auto &tracks = slide->animations();
    buildAnimationData(tracks, &animationDataList);
}

void Q3DSAnimationManagerNg::updateDynamicKeyFrames(Q3DSSlide *slide)
{
    if (!slide)
        return;

    Q3DSSlideAttached *data = slide->attached<Q3DSSlideAttached>();
    Q_ASSERT(data);

    if (!data->animatorNg)
        return;

    static const auto updateKf = [](float value, Q3DSAnimationData::KeyFrameData *kf) {
        const float dC1v = kf->c1.value - kf->value;
        const float dC2v = kf->c2.value - kf->value;
        kf->value = value;
        kf->c1.value = value + dC1v;
        kf->c2.value = value + dC2v;
    };

    auto &animationDataList = data->animatorNg->animationDataList;
    auto it = animationDataList.begin();
    const auto end = animationDataList.end();
    while (it != end) {
        if (it->type == Q3DSAnimationData::DataType::Component && (it->componentData.componentFlags & Q3DSAnimationData::ComponentFlags::Dynamic)) {
            // find the target and the components that needs to be updated
            QVarLengthArray<Q3DSAnimationData *, 3> components;
            const auto targetIt = std::find_if(it, end, [&components](Q3DSAnimationData &data) {
                if (data.type == Q3DSAnimationData::DataType::Target)
                    return true;
                else if (data.type == Q3DSAnimationData::DataType::Component && (data.componentData.componentFlags & Q3DSAnimationData::ComponentFlags::Dynamic))
                    components.push_back(&data);
                return false;
            });

            Q_ASSERT(targetIt != end && targetIt->type == Q3DSAnimationData::DataType::Target);
            const auto vec3 = toVector3D(Q3DSGraphObject::readProperty(targetIt->targetData.target, targetIt->targetData.pid));
            // Update the first kf on each track
            for (auto &component : components) {
                if (component == nullptr)
                    break;

                Q_ASSERT(component->type == Q3DSAnimationData::DataType::Component);
                if (component->componentData.size < 1) // No key-frames...
                    continue;

                auto kfData = (component + 1);
                Q_ASSERT(kfData->type == Q3DSAnimationData::DataType::KeyFrame);
                auto &kf = kfData->keyFrameData;
                const float value = vec3[static_cast<int>(component->componentData.component)];
                updateKf(value, &kf);
            }
            it = targetIt;
        }
        ++it;
    }
}

void Q3DSAnimationManagerNg::setActive(Q3DSSlide *slide, bool active)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->active = active;
}

void Q3DSAnimationManagerNg::goToTime(Q3DSSlide *slide, float timeMs, bool sync)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->goToTime(timeMs / 1000.f, sync);
}

void Q3DSAnimationManagerNg::setRate(Q3DSSlide *slide, float rate)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->rate = rate;
}

void Q3DSAnimationManagerNg::setDuration(Q3DSSlide *slide, float durationMs)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->duration = durationMs / 1000.f;
}

void Q3DSAnimationManagerNg::setPlayMode(Q3DSSlide *slide, Q3DSSlide::PlayMode mode)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->playMode = mode;
}

void Q3DSAnimationManagerNg::setEosCallback(Q3DSSlide *slide, Q3DSAnimator::EosCallback cb)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->eosCallback = cb;
}

void Q3DSAnimationManagerNg::setTimeChangeCallback(Q3DSSlide *slide, Q3DSAnimator::TimeChangeCallback cb)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->timeChangeCallback = cb;
}

void Q3DSAnimationManagerNg::advance(Q3DSSlide *slide, float dtMs)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg)
        slide->attached<Q3DSSlideAttached>()->animatorNg->advance(dtMs / 1000.f);
}

void Q3DSAnimationManagerNg::addObject(Q3DSSlide *slide, Q3DSGraphObject *obj)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg) {
        auto &animationDataList = slide->attached<Q3DSSlideAttached>()->animatorNg->animationDataList;
        const auto tracks = slide->animations();
        Q3DSSlide::AnimationTrackList tracksForObject;
        auto cit = tracks.cbegin();
        while (cit != tracks.cend()) {
            if (cit->target() == obj)
                tracksForObject.push_back(*cit);
            ++cit;
        }
        buildAnimationData(tracksForObject, &animationDataList);
    }
}

void Q3DSAnimationManagerNg::removeObject(Q3DSSlide *slide, Q3DSGraphObject *obj)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg) {
        Q3DSAnimator *animator = slide->attached<Q3DSSlideAttached>()->animatorNg;
        auto &dl = animator->animationDataList;

        auto it = dl.begin();
        const auto end = dl.end();
        auto removeStart = it;
        while (it != end) {
            if (it->type == Q3DSAnimationData::DataType::Target) {
                if (it->targetData.target == obj)
                    break;
                removeStart = it + 1;
                Q_ASSERT(removeStart == end || removeStart->type == Q3DSAnimationData::DataType::Component);
            }
            ++it;
        }

        if (it != end) {
            Q_ASSERT(removeStart->type == Q3DSAnimationData::DataType::Component && it->type == Q3DSAnimationData::DataType::Target);
            dl.erase(removeStart, it + 1);
        }
    }
}

void Q3DSAnimationManagerNg::destroyAnimator(Q3DSSlide *slide)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg) {
        delete slide->attached<Q3DSSlideAttached>()->animatorNg;
        slide->attached<Q3DSSlideAttached>()->animatorNg = nullptr;
    }
}

void Q3DSAnimationManagerNg::setTrackEnabled(Q3DSSlide *slide, const Q3DSAnimationTrack &track, bool enabled)
{
    if (slide && slide->attached<Q3DSSlideAttached>()->animatorNg) {
        Q3DSAnimator *animator = slide->attached<Q3DSSlideAttached>()->animatorNg;
        auto &dl = animator->animationDataList;

        const auto target = track.target();
        const auto propertyStr = track.property().split('.').at(0);
        const int pid = Q3DSGraphObject::indexOfProperty(target, propertyStr.toLatin1());
        Q_ASSERT(pid != -1);
        auto it = dl.begin();
        const auto end = dl.end();
        auto start = it;
        while (it != end) {
            if (it->type == Q3DSAnimationData::DataType::Target) {
                if (it->targetData.target == target && it->targetData.pid == pid)
                    break;
                start = it + 1;
                Q_ASSERT(start == end || start->type == Q3DSAnimationData::DataType::Component);
            }
            ++it;
        }

        if (it != end) {
            Q_ASSERT(start->type == Q3DSAnimationData::DataType::Component && it->type == Q3DSAnimationData::DataType::Target);
            const auto back = it - 1;
            while (start != back) {
                if (start->type == Q3DSAnimationData::DataType::Component) {
                    start->componentData.componentFlags = enabled ? Q3DSAnimationData::ComponentFlags(start->componentData.componentFlags & (~Q3DSAnimationData::ComponentFlags::Disabled))
                                                                  : Q3DSAnimationData::ComponentFlags(start->componentData.componentFlags | Q3DSAnimationData::ComponentFlags::Disabled);
                }
                ++start;
            }
        }
    }
}

QT_END_NAMESPACE

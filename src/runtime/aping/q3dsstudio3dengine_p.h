/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSSTUDIO3DENGINE_P_H
#define Q3DSSTUDIO3DENGINE_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DStudioRuntime2/private/q3dsruntimeglobal_p.h>
#include <QMutex>
#include <QElapsedTimer>
#include <QQuickItem>
#include <Qt3DCore/QNodeId>

QT_BEGIN_NAMESPACE

class Q3DSEngine;
class Q3DSUipPresentation;
class Q3DSScene;
class Q3DSSlide;
class QOpenGLFramebufferObject;
class Q3DSLayer3D;

namespace Qt3DCore {
class QAspectEngine;
}
namespace Qt3DRender {
class QRenderAspect;
class QRenderAspectPrivate;
}

class Q3DSV_PRIVATE_EXPORT Q3DSStudio3DEngine : public QQuickItem
{
    Q_OBJECT

public:
    explicit Q3DSStudio3DEngine(QQuickItem *parent = 0);
    ~Q3DSStudio3DEngine();

    void registerView(Q3DSLayer3D *view);
    void unregisterView(Q3DSLayer3D *view);
    void handleViewGeometryChange(Q3DSLayer3D *view, const QSize &size);

    Q3DSUipPresentation *presentation() const { return m_presentation; }
    Q3DSEngine *engine() const { return m_engine; }

private slots:
    void startEngine();
    void destroyEngine();
    void updateViews();

private:
    void itemChange(QQuickItem::ItemChange change, const QQuickItem::ItemChangeData &changeData) override;
    void releaseResources() override;

    void createEngine();
    void sendResizeToQt3D(const QSize &size, qreal dpr);
    void releaseEngineAndRenderer();

    Q3DSEngine *m_engine = nullptr;
    Q3DSUipPresentation *m_presentation = nullptr;
    Q3DSScene *m_scene = nullptr;
    Q3DSSlide *m_masterSlide = nullptr;
    Q3DSSlide *m_slide = nullptr;

    class Renderer : public QObject {
    public:
        Renderer(Q3DSStudio3DEngine *engine, Qt3DCore::QAspectEngine *aspectEngine);
        ~Renderer();
        void invalidateEngine();
        void notifyEngineStart();

        struct Layer {
            Q3DSLayer3D *item = nullptr;
            Qt3DCore::QNodeId textureNodeId;
            QSize pixelSize;
            int sampleCount;
            QSize itemSizeWithoutDpr;
        };

        void setLayers(const QVector<Layer> &layers) { m_layers = layers; }

    private:
        void render();

        Q3DSStudio3DEngine *m_engine;
        bool m_engineInvalid = false;
        QMutex m_engineInvalidLock;
        QAtomicInt m_engineStarted;
        Qt3DCore::QAspectEngine *m_aspectEngine;
        Qt3DRender::QRenderAspect *m_renderAspect = nullptr;
        Qt3DRender::QRenderAspectPrivate *m_renderAspectD = nullptr;
        QOpenGLFramebufferObject *m_fbo = nullptr;
        QVector<Layer> m_layers;
        QElapsedTimer m_renderTimer;
        bool m_usingRenderThread;
    };
    Renderer *m_renderer = nullptr;

    QVector<Q3DSLayer3D *> m_views;
    bool m_pendingViewSend = false;
};

QT_END_NAMESPACE

#endif // Q3DSSTUDIO3DENGINE_P_H

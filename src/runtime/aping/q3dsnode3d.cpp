/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsnode3d_p.h"

QT_BEGIN_NAMESPACE

/*!
    \qmltype Node3D
    //! \instantiates Q3DSNode3D
    \inqmlmodule QtStudio3D
    \ingroup 3dstudioruntime2
    \inherits Object3D
    \since Qt 3D Studio 2.1

    \internal
 */

Q3DSNode3D::Q3DSNode3D()
{
}

Q3DSNode3D::~Q3DSNode3D()
{
}

void Q3DSNode3D::setScale(const QVector3D &scale)
{
    if (m_scale == scale)
        return;

    m_scale = scale;
    Q_EMIT scaleChanged(m_scale);
}

void Q3DSNode3D::setRotation(const QVector3D &rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    Q_EMIT rotationChanged(m_rotation);
}

void Q3DSNode3D::setPosition(const QVector3D &position)
{
    if (m_position == position)
        return;

    m_position = position;
    Q_EMIT positionChanged(m_position);
}

void Q3DSNode3D::setPivot(const QVector3D &pivot)
{
    if (m_pivot == pivot)
        return;

    m_pivot = pivot;
    Q_EMIT pivotChanged(m_pivot);
}

QT_END_NAMESPACE

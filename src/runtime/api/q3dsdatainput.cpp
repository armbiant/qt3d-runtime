/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsdatainput_p.h"
#include "q3dspresentation.h"
#include "q3dspresentation_p.h"

QT_BEGIN_NAMESPACE

/*!
    \class Q3DSDataInput
    \inmodule Qt3DStudioRuntime2
    \since Qt 3D Studio 2.0

    \brief Controls a data input entry in a Qt 3D Studio presentation.

    This class is a convenience class for controlling a data input in a presentation.

    \sa Q3DSPresentation
*/

/*!
    \internal
 */
Q3DSDataInput::Q3DSDataInput(QObject *parent)
    : QObject(*new Q3DSDataInputPrivate, parent)
{
}

/*!
    Constructs a Q3DSDataInput instance and initializes the \a name. The
    constructed instance is automatically associated with the specified \a
    presentation. An optional \a parent object can be specified.
 */
Q3DSDataInput::Q3DSDataInput(Q3DSPresentation *presentation, const QString &name, QObject *parent)
    : QObject(*new Q3DSDataInputPrivate, parent)
{
    Q_D(Q3DSDataInput);
    d->name = name;
    d->presentation = presentation;
}

/*!
    \internal
 */
Q3DSDataInput::Q3DSDataInput(Q3DSDataInputPrivate &dd, QObject *parent)
    : QObject(dd, parent)
{
}

/*!
    Destructor.
 */
Q3DSDataInput::~Q3DSDataInput()
{
}

/*!
    \property Q3DSDataInput::name

    Specifies the name of the controlled data input element in the
    presentation. This property must be set before setting the value property.
    The initial value is provided via the constructor in practice, but the name
    can also be changed later on, if desired.
 */
QString Q3DSDataInput::name() const
{
    Q_D(const Q3DSDataInput);
    return d->name;
}

void Q3DSDataInput::setName(const QString &name)
{
    Q_D(Q3DSDataInput);
    if (d->name != name) {
        d->name = name;
        emit nameChanged();
    }
}

/*!
    \property Q3DSDataInput::value

    Specifies the value of the controlled data input element in the
    presentation.

    The value of this property only accounts for changes done via the same
    Q3DSDataInput instance. If the value of the same data input in the
    presentation is changed elsewhere, for example via animations or
    Q3DSPresentation::setAttribute(), those changes are not reflected in the
    value of this property. Due to this uncertainty, this property treats all
    value sets as changes even if the newly set value is the same value as the
    previous value.
*/
QVariant Q3DSDataInput::value() const
{
    Q_D(const Q3DSDataInput);
    return d->value;
}

/*!
    Returns true if presentation (or its subpresentation) associated with
    this data input has a data input definition with a matching name. Returns
    false if the data input has no associated presentation, or if a match is not found.
 */
bool Q3DSDataInput::isValid() const
{
    Q_D(const Q3DSDataInput);
    // Fast exit if name or presentation is null
    if (name().isEmpty() || !d->presentation) {
        return false;
    } else {
        return Q3DSPresentationPrivate::get(d->presentation)
                ->controller->handleIsValidDataInput(this);
    }
}

/*!
    Returns the metadata for this data input for the specified \a key. Metadata is convenience
    data that can be used by API clients to better identify data input purpose, and facilitate
    binding the appropriate external data source to this data input. Metadata does not have
    any impact to rendering.

    \sa Q3DSDataInput::metadataKeys()
 */
QVariant Q3DSDataInput::metadata(const QVariant &key) const
{
    Q_D(const Q3DSDataInput);
    if (!d->presentation)
        return {};

    return Q3DSPresentationPrivate::get(d->presentation)
            ->controller->handleDataInputMetaData(this, key);
}

/*!
    Returns the metadata keys for this data input.

    \sa Q3DSDataInput::metadata()

 */
QVariantList Q3DSDataInput::metadataKeys() const
{
    Q_D(const Q3DSDataInput);
    if (!d->presentation)
        return {};

    return QVariantList(Q3DSPresentationPrivate::get(d->presentation)
                        ->controller->handleDataInputMetaDataKeys(this));
}

/*!
    Removes the metadata for this data input for the specified \a key.

    \sa Q3DSDataInput::metadata()
 */
void Q3DSDataInput::removeMetadata(const QVariant &key)
{
    Q_D(Q3DSDataInput);
    d->sendMetadataRemove(key);
}

/*!
    Sets the \a metadata for this data input for the specified \a key.

    \sa Q3DSDataInput::metadata()
 */
void Q3DSDataInput::setMetadata(const QVariant &key, const QVariant &metadata)
{
    Q_D(Q3DSDataInput);
    d->sendMetaData(key, metadata);
}

void Q3DSDataInput::setValue(const QVariant &value)
{
    Q_D(Q3DSDataInput);
    // Since properties controlled by data inputs can change without the
    // current value being reflected on the value of the DataInput element, we
    // allow setting the value to the same one it was previously and still
    // consider it a change. For example, when controlling timeline, the value
    // set to DataInput will only be the current value for one frame if
    // presentation has a running animation.
    d->value = value;
    d->sendValue();
    emit valueChanged();
}

/*!
   \property Q3DSDataInput::min

    Returns the minimum range value for data input. Returned value is zero
    for data input types other than \e {Ranged Number}.
 */
float Q3DSDataInput::min() const
{
    Q_D(const Q3DSDataInput);
    if (!d->presentation)
        return 0.0f;

    return Q3DSPresentationPrivate::get(d->presentation)->dataInputMin(d->name);
}

/*!
    Sets the minimum (\a min) range value for data input.

    This property is applicable only to data input type \e {Ranged Number}.
 */
void Q3DSDataInput::setMin(float min)
{
    Q_D(Q3DSDataInput);
    d->sendMin(min);
    emit minChanged();
}

/*!
    \property Q3DSDataInput::max

    Returns the maximum (\a max) range value for data input. Returned value is zero
    for data input types other than \e {Ranged Number}.
 */
float Q3DSDataInput::max() const
{
    Q_D(const Q3DSDataInput);

    if (!d->presentation)
        return 0.0f;

    return Q3DSPresentationPrivate::get(d->presentation)->dataInputMax(d->name);
}

/*!
    Sets the maximum (\a max) range value for data input.

    This property is applicable only to data input type \e {Ranged Number}.
 */
void Q3DSDataInput::setMax(float max)
{
    Q_D(Q3DSDataInput);
    d->sendMax(max);
    emit maxChanged();
}

void Q3DSDataInputPrivate::sendMetaData(const QVariant &key, const QVariant &metaData)
{
    if (!presentation || key.isNull())
        return;

    Q3DSPresentationPrivate::get(presentation)->setDataInputMetaData(name, key, metaData);
}

void Q3DSDataInputPrivate::sendValue()
{
    if (!presentation || name.isEmpty())
        return;

    presentation->setDataInputValue(name, value);
}

void Q3DSDataInputPrivate::sendMin(float min)
{
    if (!presentation)
        return;

    Q3DSPresentationPrivate::get(presentation)->setDataInputMin(name, min);
}

void Q3DSDataInputPrivate::sendMax(float max)
{
    if (!presentation)
        return;

    Q3DSPresentationPrivate::get(presentation)->setDataInputMax(name, max);
}

void Q3DSDataInputPrivate::sendMetadataRemove(const QVariant &key)
{
    if (!presentation)
        return;

    Q3DSPresentationPrivate::get(presentation)->removeDatainputMetadata(name, key);
}

/*!
    \qmltype DataInput
    \instantiates Q3DSDataInput
    \inqmlmodule QtStudio3D
    \ingroup 3dstudioruntime2

    \brief Controls a data input entry in a Qt 3D Studio presentation.

    This type is a convenience for controlling a data input in a presentation.
    Its functionality is equivalent to Presentation::setDataInputValue(),
    however it has a big advantage of being able to use QML property bindings,
    thus avoiding the need to having to resort to a JavaScript function call
    for every value change.

    As an example, compare the following two approaches:

    \qml
        Studio3D {
            ...
            Presentation {
                id: presentation
                ...
            }
        }

        Button {
            onClicked: presentation.setAttribute("SomeTextNode", "textstring", "Hello World")
        }
    \endqml

    \qml
        Studio3D {
            ...
            Presentation {
                id: presentation
                ...
                property string text: ""
                DataInput {
                    name: "inputForSomeTextNode"
                    value: presentation.text
                }
            }
        }

        Button {
            onClicked: presentation.text = "Hello World"
        }
    \endqml

    The latter assumes that a data input connection was made in Qt 3D Studio
    between the \c textstring property of \c SomeTextNode and a data input name
    \c inputForSomeTextNode. As the value is now set via a property, the full
    set of QML property bindings techniques are available.

    \sa Studio3D, Presentation
*/

/*!
    \qmlproperty string DataInput::name

    Specifies the name of the controlled data input element in the
    presentation. This property must be set as part of DataInput declaration,
    although it is changeable afterwards, if desired.
*/

/*!
    \qmlproperty variant DataInput::value

    Specifies the value of the controlled data input element in the presentation.

    The value of this property only accounts for changes done via the same
    DataInput instance. If the value of the underlying attribute in the
    presentation is changed elsewhere, for example via animations or
    Presentation::setAttribute(), those changes are not reflected in the value
    of this property. Due to this uncertainty, this property treats all value
    sets as changes even if the newly set value is the same value as the
    previous value.
*/

/*!
    \qmlproperty real DataInput::min

    Specifies the minimum value of the controlled data input element range.

    This property is applicable only to data input type \e {Ranged Number}. For other
    types, value returned is zero.

    The value of this property only accounts for changes done via the same
    DataInput instance. If the value of the property is changed elsewhere,
    those changes are not reflected in the value of this property.
    Due to this uncertainty, this property treats all value sets as changes
    even if the newly set value is the same value as the
    previous value.
*/

/*!
    \qmlproperty real DataInput::max

    Specifies the maximum value of the controlled data input element range.

    This property is applicable only to data input type \e {Ranged Number}. For other
    types, value returned is zero.

    The value of this property only accounts for changes done via the same
    DataInput instance. If the value of the property is changed elsewhere,
    those changes are not reflected in the value of this property.
    Due to this uncertainty, this property treats all value sets as changes
    even if the newly set value is the same value as the
    previous value.
*/

/*!
    \qmlmethod void DataInput::setMetadata(QVariant key, QVariant metadata)

    Sets the data input \a metadata for \a key.
*/

/*!
    \qmlmethod QVariant DataInput::getMetadata(QVariant key)

    Gets the data input metadata for \a key.
*/

/*!
    \qmlmethod QVariantList DataInput::getMetadataKeys()

    Gets all metadata keys for this data input.
*/

/*!
    \qmlmethod void DataInput::removeMetadata(QVariant key)

    Removes data input metadata for /a key.
*/

QT_END_NAMESPACE

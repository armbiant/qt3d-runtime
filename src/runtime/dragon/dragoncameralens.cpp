/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragoncameralens_p.h"

#include <Qt3DCore/qpropertyupdatedchange.h>

#include <Qt3DRender/private/qcameralens_p.h>
#include <Qt3DCore/private/vector3d_p.h>
#include <Qt3DCore/private/vector4d_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

void CameraLens::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QCameraLensData>>(change);
    const auto &data = typedChange->data;
    m_projectionMatrix = Matrix4x4(data.projectionMatrix);
    m_exposure = data.exposure;
}

Matrix4x4 CameraLens::viewMatrix(const Matrix4x4 &worldTransform) const
{
    const Vector4D position = worldTransform * Vector4D(0.0f, 0.0f, 0.0f, 1.0f);
    // OpenGL convention is looking down -Z
    const Vector4D viewDirection = worldTransform * Vector4D(0.0f, 0.0f, -1.0f, 0.0f);
    const Vector4D upVector = worldTransform * Vector4D(0.0f, 1.0f, 0.0f, 0.0f);

    QMatrix4x4 m;
    m.lookAt(convertToQVector3D(Vector3D(position)),
             convertToQVector3D(Vector3D(position + viewDirection)),
             convertToQVector3D(Vector3D(upVector)));
    return Matrix4x4(m);
}

void CameraLens::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    switch (e->type()) {
    case PropertyUpdated: {
        QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);

        if (propertyChange->propertyName() == QByteArrayLiteral("projectionMatrix")) {
            QMatrix4x4 projectionMatrix = propertyChange->value().value<QMatrix4x4>();
            m_projectionMatrix = Matrix4x4(projectionMatrix);
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("exposure")) {
            m_exposure = propertyChange->value().toFloat();
            markDirty();
        }

        break;
    }

        // TODO Consider implementing CommandRequested
//    case CommandRequested: {
//        QNodeCommandPtr command = qSharedPointerCast<QNodeCommand>(e);

//        if (command->name() == QLatin1Literal("QueryRootBoundingVolume")) {
//            m_pendingViewAllCommand = command->commandId();
//            QVariant v = command->data();
//            QNodeId id = v.value<QNodeId>();
//            computeSceneBoundingVolume({}, id, command->commandId());
//        } else if (command->name() == QLatin1Literal("QueryEntityBoundingVolume")) {
//            m_pendingViewAllCommand = command->commandId();
//            QVariant v = command->data();
//            QVector<QNodeId> ids = v.value<QVector<QNodeId>>();
//            if (ids.size() == 2)
//                computeSceneBoundingVolume(ids[0], ids[1], command->commandId());
//        }
//    }
//        break;

    default:
        break;
    }
    BackendNode::sceneChangeEvent(e);
}

Matrix4x4 CameraLens::projectionMatrix() const
{
    return m_projectionMatrix;
}

Matrix4x4 CameraLens::projection() const
{
    return m_projectionMatrix;
}

float CameraLens::exposure() const
{
    return m_exposure;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONENTITY_P_H
#define DRAGONENTITY_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonbackendnode_p.h>

#include <Qt3DCore/QNodeIdTypePair>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class Entity : public BackendNode
{
public:
    Entity();

    Qt3DCore::QNodeId theId;

    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) override;

    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;

    void addComponent(Qt3DCore::QNodeIdTypePair idAndType);
    void removeComponent(Qt3DCore::QNodeId nodeId);

    Qt3DCore::QNodeId parentId() const;
    Qt3DCore::QNodeId cameraLensComponent() const;

    Qt3DCore::QNodeId m_materialComponent;
    QVector<Qt3DCore::QNodeId> m_layerComponents;
    Qt3DCore::QNodeId m_transformComponent;
    Qt3DCore::QNodeId m_geometryRendererComponent;
    bool m_enabled = true;

private:
    Qt3DCore::QNodeId m_parentEntityId;
    Qt3DCore::QNodeId m_cameraLensComponent;
    QVector<Qt3DCore::QNodeId> m_levelOfDetailComponents;
    QVector<Qt3DCore::QNodeId> m_rayCasterComponents;
    QVector<Qt3DCore::QNodeId> m_shaderDataComponents;
    QVector<Qt3DCore::QNodeId> m_lightComponents;
    QVector<Qt3DCore::QNodeId> m_environmentLightComponents;
    Qt3DCore::QNodeId m_objectPickerComponent;
    Qt3DCore::QNodeId m_boundingVolumeDebugComponent;
    Qt3DCore::QNodeId m_computeComponent;
    Qt3DCore::QNodeId m_armatureComponent;
};
} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // DRAGONENTITY_P_H

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragontexture_p.h"

#include <private/dragontextureimage_p.h>
#include <private/dragoncomparegenerators_p.h>

#include <Qt3DRender/private/qabstracttexture_p.h>
#include <Qt3DCore/QPropertyUpdatedChange>
#include <Qt3DCore/QPropertyNodeAddedChange>
#include <Qt3DCore/QPropertyNodeRemovedChange>

QT_BEGIN_NAMESPACE
using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

void Texture::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    DirtyFlags dirty = NotDirty;

    switch (e->type()) {
    case PropertyUpdated: {
        QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("width")) {
            properties.width = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("height")) {
            properties.height = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("depth")) {
            properties.depth = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("maximumLayers")) {
            properties.layers = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("format")) {
            properties.format = static_cast<QAbstractTexture::TextureFormat>(propertyChange->value().toInt());
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("target")) {
            properties.target = static_cast<QAbstractTexture::Target>(propertyChange->value().toInt());
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("mipmaps")) {
            properties.generateMipMaps = propertyChange->value().toBool();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("minificationFilter")) {
            parameters.minificationFilter = static_cast<QAbstractTexture::Filter>(propertyChange->value().toInt());
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("magnificationFilter")) {
            parameters.magnificationFilter = static_cast<QAbstractTexture::Filter>(propertyChange->value().toInt());
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("wrapModeX")) {
            parameters.wrapModeX = static_cast<QTextureWrapMode::WrapMode>(propertyChange->value().toInt());
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("wrapModeY")) {
            parameters.wrapModeY = static_cast<QTextureWrapMode::WrapMode>(propertyChange->value().toInt());
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("wrapModeZ")) {
            parameters.wrapModeZ =static_cast<QTextureWrapMode::WrapMode>(propertyChange->value().toInt());
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("maximumAnisotropy")) {
            parameters.maximumAnisotropy = propertyChange->value().toFloat();
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("comparisonFunction")) {
            parameters.comparisonFunction = propertyChange->value().value<QAbstractTexture::ComparisonFunction>();
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("comparisonMode")) {
            parameters.comparisonMode = propertyChange->value().value<QAbstractTexture::ComparisonMode>();
            dirty = DirtyParameters;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("layers")) {
            properties.layers = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("samples")) {
            properties.samples = propertyChange->value().toInt();
            dirty = DirtyProperties;
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("generator")) {
            generator = propertyChange->value().value<QTextureGeneratorPtr>();
            dirty = DirtyDataGenerator;
            markDirty();
        }
    }
        break;

        // TODO these are not used because a texture image must be a child of a texture to work,
        // due to a limitation in the change arbiter not being able to pick up changes at construction time
        // Consider fixing the change arbiter by using the new change system.

//    case PropertyValueAdded: {
//        const auto change = qSharedPointerCast<QPropertyNodeAddedChange>(e);
//        if (change->propertyName() == QByteArrayLiteral("textureImage")) {
//            textureImages.append(change->addedNodeId());
//        }
//    }
//        break;

//    case PropertyValueRemoved: {
//        const auto change = qSharedPointerCast<QPropertyNodeRemovedChange>(e);
//        if (change->propertyName() == QByteArrayLiteral("textureImage")) {
//            textureImages.remove(change->removedNodeId());
//        }
//    }
//        break;

    default:
        break;

    }

    addDirtyFlag(dirty);
    BackendNode::sceneChangeEvent(e);
}

void Texture::addDirtyFlag(DirtyFlags flags)
{
    dirty |= flags;

    // TODO consider making a flag part of the markDirty function
    markDirty();
}

void Texture::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QAbstractTextureData>>(change);
    const auto &data = typedChange->data;

    properties.target = data.target;
    properties.format = data.format;
    properties.width = data.width;
    properties.height = data.height;
    properties.depth = data.depth;
    properties.generateMipMaps = data.autoMipMap;
    properties.layers = data.layers;
    properties.samples = data.samples;
    parameters.minificationFilter = data.minFilter;
    parameters.magnificationFilter = data.magFilter;
    parameters.wrapModeX = data.wrapModeX;
    parameters.wrapModeY = data.wrapModeY;
    parameters.wrapModeZ = data.wrapModeZ;
    parameters.maximumAnisotropy = data.maximumAnisotropy;
    parameters.comparisonFunction = data.comparisonFunction;
    parameters.comparisonMode = data.comparisonMode;
    generator = data.dataFunctor;

    addDirtyFlag(DirtyFlags(DirtyImageGenerators|DirtyProperties|DirtyParameters));
}

bool operator ==(const LoadedTexture &a, const LoadedTexture &b)
{
    return a.images == b.images && a.texture == b.texture;
}

bool operator ==(const Texture &a, const Texture &b)
{
    return compareGenerators(a.generator, b.generator) && a.properties == b.properties && a.parameters == b.parameters;
}

}
}
QT_END_NAMESPACE

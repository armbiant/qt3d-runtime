/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonbuffer_p.h"

#include <private/dragoncomparegenerators_p.h>

#include <Qt3DCore/qpropertyupdatedchange.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

void Buffer::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QBufferData>>(change);
    const auto &data = typedChange->data;
    m_data = data.data;
    m_usage = data.usage;
    m_access = data.access;
    m_generator = data.functor;

    if (data.syncData)
        qWarning() << "WARNING: Dragon render aspect does not yet support syncData. Data on frontend node will not be updated.";
}

void Buffer::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    if (e->type() == PropertyUpdated) {
        QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);
        QByteArray propertyName = propertyChange->propertyName();
        if (propertyName == QByteArrayLiteral("data")) {
            m_data = propertyChange->value().toByteArray();
            markDirty();
        } else if (propertyName == QByteArrayLiteral("updateData")) {
            qWarning() << "WARNING: Dragon render aspect does not yet support updateData. Buffer data will not be updated.";
            markDirty();
        } else if (propertyName == QByteArrayLiteral("usage")) {
            m_usage = static_cast<QBuffer::UsageType>(propertyChange->value().value<int>());
            markDirty();
        } else if (propertyName == QByteArrayLiteral("accessType")) {
            m_access = static_cast<QBuffer::AccessType>(propertyChange->value().value<int>());
            markDirty();
        } else if (propertyName == QByteArrayLiteral("dataGenerator")) {
            QBufferDataGeneratorPtr newGenerator = propertyChange->value().value<QBufferDataGeneratorPtr>();
            m_generator = newGenerator;
            markDirty();
        } else if (propertyName == QByteArrayLiteral("syncData")) {
            if (propertyChange->value().toBool())
                qWarning() << "WARNING: Dragon render aspect does yet not support syncData. Data on frontend node will not be updated.";
            markDirty();
        }
    }
    BackendNode::sceneChangeEvent(e);
}

bool operator ==(const Buffer &a, const Buffer &b)
{
    return compareGenerators(a.m_generator, b.m_generator) && a.m_access == b.m_access && a.m_data == b.m_data
            && a.m_usage == b.m_usage;
}

} // Dragon
} // Qt3DRender

QT_END_NAMESPACE

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragongeometry_p.h"

#include <Qt3DRender/qgeometry.h>
#include <Qt3DRender/private/qgeometry_p.h>

#include <Qt3DCore/qpropertyupdatedchange.h>
#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertynoderemovedchange.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

Geometry::Geometry()
    : BackendNode()
    , m_geometryDirty(false)
{
}

void Geometry::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QGeometryData>>(change);
    const auto &data = typedChange->data;
    m_attributes = data.attributeIds;
    m_boundingPositionAttribute = data.boundingVolumePositionAttributeId;
    m_geometryDirty = true;
//    markDirty(AbstractRenderer::GeometryDirty);
}

void Geometry::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    switch (e->type()) {
    case PropertyValueAdded: {
        const auto change = qSharedPointerCast<QPropertyNodeAddedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("attribute")) {
            m_attributes.push_back(change->addedNodeId());
            m_geometryDirty = true;
            markDirty();
        }
        break;
    }

    case PropertyValueRemoved: {
        const auto change = qSharedPointerCast<QPropertyNodeRemovedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("attribute")) {
            m_attributes.removeOne(change->removedNodeId());
            m_geometryDirty = true;
            markDirty();
        }
        break;
    }

    case PropertyUpdated: {
        // Note: doesn't set dirtyness as this parameter changing doesn't need a new VAO update.
        const auto change = qSharedPointerCast<QPropertyUpdatedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("boundingVolumePositionAttribute")) {
            m_boundingPositionAttribute = change->value().value<QNodeId>();
            markDirty();
            break;
        }
    }

    default:
        break;
    }
//    markDirty(AbstractRenderer::GeometryDirty);
    BackendNode::sceneChangeEvent(e);
}

void Geometry::unsetDirty()
{
    m_geometryDirty = false;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

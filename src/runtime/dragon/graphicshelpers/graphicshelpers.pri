#DEFINES += QT3D_RENDER_ASPECT_OPENGL_DEBUG

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/dragongraphicshelperinterface_p.h \
    $$PWD/dragongraphicshelperes2_p.h \
    $$PWD/dragongraphicshelperes3_1_p.h \
    $$PWD/dragongraphicshelperes3_2_p.h \
    $$PWD/dragongraphicshelperes3_p.h \
    $$PWD/dragongraphicshelpergl2_p.h \
    $$PWD/dragongraphicshelpergl3_2_p.h \
    $$PWD/dragongraphicshelpergl3_3_p.h \
    $$PWD/dragongraphicshelpergl4_p.h

SOURCES += \
    $$PWD/dragongraphicshelperes2.cpp \
    $$PWD/dragongraphicshelperes3.cpp \
    $$PWD/dragongraphicshelperes3_1.cpp \
    $$PWD/dragongraphicshelperes3_2.cpp \
    $$PWD/dragongraphicshelpergl2.cpp \
    $$PWD/dragongraphicshelpergl3_2.cpp \
    $$PWD/dragongraphicshelpergl3_3.cpp \
    $$PWD/dragongraphicshelpergl4.cpp

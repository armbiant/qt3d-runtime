/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_RENDERSTATE_H
#define QT3DRENDER_DRAGON_RENDERSTATE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragongenericstate_p.h>
#include <private/dragonrenderstates_p.h>
#include <private/dragonstatevariant_p.h>
#include <private/dragonimmutable_p.h>

#include <QVector>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

class QRenderState;

namespace Dragon {

class RenderStateSet
{
public:
    RenderStateSet();
    ~RenderStateSet();

    template<typename GenericState>
    void addState(const GenericState &state)
    {
        addState(StateVariant::fromValue(state));
    }

    /**
     * @brief changeCost - metric of cost to change to this state-set from
     * a candidate previous state-set. This is used to find an optimal
     * ordering of state-sets when sending draw commands.
     * @param previousState
     * @return
     */
    int changeCost(RenderStateSet* previousState);

    StateMaskSet stateMask() const;

    const QHash<StateMask, StateVariant> &states() const { return m_states; }

    /**
     * @brief contains - check if this set contains a matching piece of state
     * @param ds
     * @return
     */
    bool contains(const StateVariant &ds) const;

    static RenderStateSet defaultRenderStateSet();
private:
    StateMaskSet m_stateMask;
    QHash<StateMask, StateVariant> m_states;
};

template<>
void RenderStateSet::addState<StateVariant>(const StateVariant &state);

} // namespace Render
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_RENDERSTATE_H

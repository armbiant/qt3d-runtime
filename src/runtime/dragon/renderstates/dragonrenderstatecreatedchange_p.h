/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONRENDERSTATECREATEDCHANGE_P_H
#define DRAGONRENDERSTATECREATEDCHANGE_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DCore/qnodecreatedchange.h>

#include <private/dragonstatemask_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

class QRenderState;

namespace Dragon {

class RenderStateCreatedChangeBasePrivate;

class RenderStateCreatedChangeBase : public Qt3DCore::QNodeCreatedChangeBase
{
public:
    explicit RenderStateCreatedChangeBase(const Qt3DRender::QRenderState *renderState);

    Dragon::StateMask renderStateType() const;

private:
    Q_DECLARE_PRIVATE(RenderStateCreatedChangeBase)
};

typedef QSharedPointer<RenderStateCreatedChangeBase> RenderStateCreatedChangeBasePtr;

template<typename T>
class RenderStateCreatedChange : public RenderStateCreatedChangeBase
{
public:
    explicit RenderStateCreatedChange(const Qt3DRender::QRenderState *_renderState)
        : RenderStateCreatedChangeBase(_renderState)
        , data()
    {
    }

    T data;
};

template<typename T>
using RenderStateCreatedChangePtr = QSharedPointer<RenderStateCreatedChange<T>>;

} // namespace Dragon

} // namespace Qt3DRender

QT_END_NAMESPACE
#endif // DRAGONRENDERSTATECREATEDCHANGE_P_H

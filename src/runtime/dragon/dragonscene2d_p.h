/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGON_SCENE2D_P_H
#define DRAGON_SCENE2D_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DCore/qnodeid.h>
#include <Qt3DCore/qpropertyupdatedchange.h>
#include <Qt3DRender/qpickevent.h>
#include <Qt3DQuickScene2D/qscene2d.h>

#include <private/qscene2d_p.h>
#include <private/dragonrendertargetoutput_p.h>
#include <private/dragonbackendnode_p.h>
#include <private/dragonattachment_p.h>
#include <private/dragontexturejobs_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

struct Scene2DData;

class RenderQmlEventHandler : public QObject
{
    Q_OBJECT
public:
    RenderQmlEventHandler(Scene2DData *node);
    bool event(QEvent *e) override;

private:
    Scene2DData *m_node;
};

struct Scene2DState
{
    Qt3DCore::QNodeId m_scene2d;
    Qt3DCore::QNodeId m_texture;
    QMutex *m_mutex = nullptr;

    Scene2DState()
    {
    }

    Scene2DState(Qt3DCore::QNodeId id, Qt3DCore::QNodeId tid, QMutex *mutex)
        : m_scene2d(id), m_texture(tid), m_mutex(mutex)
    {
    }
};

struct Scene2DData
{
    QOpenGLContext *m_context = nullptr;
    QOpenGLContext *m_shareContext = nullptr;
    QThread *m_renderThread = nullptr;
    Qt3DCore::QNodeId m_outputId;
    QSharedPointer<Qt3DRender::Quick::Scene2DSharedObject> m_sharedObject;
    Qt3DCore::QNodeId m_peerId;
    Attachment m_attachmentData;
    QMutex m_mutex;
    QSharedPointer<QOpenGLTexture> m_texture = nullptr;

    GLuint m_fbo = 0;
    GLuint m_rbo = 0;
    QSize m_textureSize;

    bool m_initialized = false;
    bool m_renderInitialized = false;
    bool m_mouseEnabled = false;
    bool m_outputDirty = true;
    Qt3DRender::Quick::QScene2D::RenderPolicy m_renderPolicy
                                                        = Qt3DRender::Quick::QScene2D::Continuous;

    void render();
    void initializeRender();
    void cleanup();
    bool updateFbo(QOpenGLTexture *texture, const QSize &size);
    void syncRenderControl();
};

class Scene2D : public BackendNode
{
public:
    Scene2D();
    Scene2D(const Scene2D &s)
        : m_data(s.m_data)
    {
    }
    ~Scene2D();

    void setSharedObject(Qt3DRender::Quick::Scene2DSharedObjectPtr sharedObject);
    void setOutput(Qt3DCore::QNodeId outputId);
    void initializeSharedObject();

    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) override;
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;

    QSharedPointer<Scene2DData> m_data;
};

ValueContainer<Scene2DState> updateScene2Ds(ValueContainer<Scene2DState> scene2dstate,
                                            const ValueContainer<Scene2D> &scene2ds,
                                            const QHash<Qt3DCore::QNodeId,
                                                   QSharedPointer<QOpenGLTexture> > *loadedTextures,
                                            const ValueContainer<RenderTargetOutput> &outputs,
                                            QOpenGLContext *shareContext);

} // Dragon
} // Qt3DRender

QT_END_NAMESPACE

#endif // DRAGON_SCENE2D_P_H

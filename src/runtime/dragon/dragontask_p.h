/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONTASK_P_H
#define DRAGONTASK_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QSharedPointer>
#include <Qt3DCore/QAspectJob>
#include <functional>

QT_BEGIN_NAMESPACE
namespace Qt3DRender {
namespace Dragon {

/*!
 * \internal
 * Simple wrapper around a value to make it easier for other jobs to
 * use tasks as input.
 *
 * TODO: Consider removing as this adds unnecessary jobs.
 */
template <typename Output>
class SourceTask : public Qt3DCore::QAspectJob
{
public:
    using result_type = Output;

    SourceTask()
        : QAspectJob()
    {
    }

    SourceTask(const SourceTask &other)
        : QAspectJob()
        , m_result(other.m_result)
    {
    }

    void setInput(Output argument)
    {
        m_hasInput = true;
        m_isFinished = true;
        m_result = argument;
    }

    void run()
    {
        Q_ASSERT(false); // the source tasks don't need to be run
    }

    const Output &result() const
    {
        Q_ASSERT(m_isFinished);
        return m_result;
    }
    bool isFinished() const { return m_isFinished; }

private:
    Output m_result;
    bool m_hasInput = false;
    bool m_isFinished = false;
};

template <typename Output>
using SourceTaskPtr = QSharedPointer<SourceTask<Output>>;

// TODO rename
// TODO consider using an enum instead
struct SelfT
{
    explicit SelfT() = default;
};

constexpr SelfT Self;

template <typename Output>
struct Task : public Qt3DCore::QAspectJob
{
    //    using Callable = std::function<Output(Output, Inputs...)>;
    using result_type = Output;

    template <typename Callable, typename... Tasks>
    Task(Callable callable, SelfT self, Tasks... tasks)
        : Qt3DCore::QAspectJob()
        , m_function(createSelfFunction(callable, tasks...))
    {
        Q_UNUSED(self);
        addDeps(tasks...);
    }

    template <typename Callable, typename... Tasks>
    Task(Callable callable, Tasks... tasks)
        : Qt3DCore::QAspectJob()
        , m_function(createFunction(callable, tasks...))
    {
        addDeps(tasks...);
    }

    Task(const Task &other)
        : Qt3DCore::QAspectJob()
        , m_function(other.m_function)
        , m_result(other.m_result)
    {
    }

    template<typename Dependency>
    void addDep(Dependency dep)
    {
        Q_UNUSED(dep);
    }

    template<typename U>
    void addDep(QSharedPointer<Task<U>> dep)
    {
        addDependency(dep);
    }

    // TODO consider getting rid of SourceTask
    template<typename U>
    void addDep(QSharedPointer<SourceTask<U>> dep)
    {
        addDependency(dep);
    }

    void addDeps() {}

    template <typename Dependency, typename... Dependencies>
    void addDeps(Dependency dep, Dependencies... deps)
    {
        Q_UNUSED(dep);
        addDep(dep);
        addDeps(deps...);
    }

    void assertTaskFinished() {}

    template <typename OtherTask, typename... Tasks>
    void assertTaskFinished(OtherTask task, Tasks... tasks)
    {
        Q_UNUSED(task);
        Q_ASSERT(task != nullptr);
        Q_ASSERT(task->isFinished());
        assertTaskFinished(tasks...);
    }

    template <typename Callable, typename... Tasks>
    std::function<void()> createSelfFunction(Callable callable, Tasks... tasks)
    {
        return [this, callable, tasks...]() {
            assertTaskFinished(tasks...);
            m_result = callable(std::move(m_result), (tasks->result())...);
        };
    }

    template <typename Callable, typename... Tasks>
    std::function<void()> createFunction(Callable callable, Tasks... tasks)
    {
        return [this, callable, tasks...]() {
            assertTaskFinished(tasks...);
            m_result = callable((tasks->result())...);
        };
    }

    void run()
    {
        m_function();

        // TODO is never reset, consider adding a function to reset after all jobs finished
        // TODO if so, take into consideration cached results
        m_isFinished = true;
    }

    const Output &result() const
    {
        Q_ASSERT(m_isFinished);
        return m_result;
    }

    bool isFinished() const { return m_isFinished; }

private:
    std::function<void()> m_function;
    Output m_result;
    bool m_isFinished = false;
};

template <typename Output>
using TaskPtr = QSharedPointer<Task<Output>>;

} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE

#endif // DRAGONTASK_P_H

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonboundingvolumejobs_p.h"

#include <Qt3DCore/QNodeId>

#include <Qt3DCore/private/matrix4x4_p.h>

#include <private/dragonentity_p.h>
#include <private/dragongeometry_p.h>
#include <private/dragongeometryrenderer_p.h>
#include <private/dragonattribute_p.h>
#include <private/dragontransform_p.h>
#include <private/dragonbuffervisitor_p.h>
#include <private/dragonjobs_common_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

namespace {

class BoundingVolumeCalculator
{
public:
    const Sphere &result() { return m_volume; }

    bool apply(const Attribute &positionAttribute,
               const LoadedBuffer &attributeBuffer,
               const Attribute &indexAttribute,
               const LoadedBuffer &indexBuffer,
               int drawVertexCount,
               bool primitiveRestartEnabled,
               int primitiveRestartIndex)
    {
        FindExtremePoints findExtremePoints;
        // TODO what about if we don't have an indexAttribute?
        if (!findExtremePoints.apply(positionAttribute, attributeBuffer, indexAttribute, indexBuffer, drawVertexCount,
                                     primitiveRestartEnabled, primitiveRestartIndex)) {
            return false;
        }

        // Calculate squared distance for the pairs of points
        const float xDist2 = (findExtremePoints.xMaxPt - findExtremePoints.xMinPt).lengthSquared();
        const float yDist2 = (findExtremePoints.yMaxPt - findExtremePoints.yMinPt).lengthSquared();
        const float zDist2 = (findExtremePoints.zMaxPt - findExtremePoints.zMinPt).lengthSquared();

        // Select most distant pair
        Vector3D p = findExtremePoints.xMinPt;
        Vector3D q = findExtremePoints.xMaxPt;
        if (yDist2 > xDist2 && yDist2 > zDist2) {
            p = findExtremePoints.yMinPt;
            q = findExtremePoints.yMaxPt;
        }
        if (zDist2 > xDist2 && zDist2 > yDist2) {
            p = findExtremePoints.zMinPt;
            q = findExtremePoints.zMaxPt;
        }

        const Vector3D c = 0.5f * (p + q);
        m_volume.setCenter(c);
        m_volume.setRadius((q - c).length());

        ExpandSphere expandSphere(m_volume);
        if (!expandSphere.apply(positionAttribute, attributeBuffer, indexAttribute, indexBuffer, drawVertexCount,
                                primitiveRestartEnabled, primitiveRestartIndex))
            return false;

        return true;
    }

private:
    Sphere m_volume;

    class FindExtremePoints : public Buffer3fVisitor
    {
    public:
        FindExtremePoints()
                : Buffer3fVisitor()
                , xMin(0.0f)
                , xMax(0.0f)
                , yMin(0.0f)
                , yMax(0.0f)
                , zMin(0.0f)
                , zMax(0.0f)
        {
        }

        float xMin, xMax, yMin, yMax, zMin, zMax;
        Vector3D xMinPt, xMaxPt, yMinPt, yMaxPt, zMinPt, zMaxPt;

        void visit(uint ndx, float x, float y, float z) override
        {
            if (ndx) {
                if (x < xMin) {
                    xMin = x;
                    xMinPt = Vector3D(x, y, z);
                }
                if (x > xMax) {
                    xMax = x;
                    xMaxPt = Vector3D(x, y, z);
                }
                if (y < yMin) {
                    yMin = y;
                    yMinPt = Vector3D(x, y, z);
                }
                if (y > yMax) {
                    yMax = y;
                    yMaxPt = Vector3D(x, y, z);
                }
                if (z < zMin) {
                    zMin = z;
                    zMinPt = Vector3D(x, y, z);
                }
                if (z > zMax) {
                    zMax = z;
                    zMaxPt = Vector3D(x, y, z);
                }
            } else {
                xMin = xMax = x;
                yMin = yMax = y;
                zMin = zMax = z;
                xMinPt = xMaxPt = yMinPt = yMaxPt = zMinPt = zMaxPt = Vector3D(x, y, z);
            }
        }
    };

    class ExpandSphere : public Buffer3fVisitor
    {
    public:
        ExpandSphere(Sphere &volume)
                : Buffer3fVisitor()
                , m_volume(volume)
        {
        }

        Sphere &m_volume;
        void visit(uint ndx, float x, float y, float z) override
        {
            Q_UNUSED(ndx);
            m_volume.expandToContain(Vector3D(x, y, z));
        }
    };
};

}

LocalBoundingVolumes calculateLocalBoundingVolumes(LocalBoundingVolumes localBoundingVolumes,
                                                   const ValueContainer<Entity> &entities,
                                                   const ValueContainer<Attribute> &attributes,
                                                   const ValueContainer<GeometryRenderer> &geometryRenderers,
                                                   const ValueContainer<Geometry> &geometries,
                                                   const ValueContainer<LoadedBuffer> &buffers)
{
    localBoundingVolumes.reset();

    if (!entities.anythingDirty() && !attributes.anythingDirty()
        && !geometryRenderers.anythingDirty() && !geometries.anythingDirty() && !buffers.anythingDirty())
        return localBoundingVolumes;

    auto calculateBoundingVolume = [=](const QNodeId &id,
                                       const Immutable<Entity> &entity) {
        // Reuse previous result as much as possible
        // TODO consider using an Optional here
        LocalBoundingVolumeResult result = *localBoundingVolumes[id];

        bool entityDirty = entities.changes().contains(Change{Change::Action::Created, id}) ||
                entities.changes().contains(Change{Change::Action::Modified, id});
        if (entityDirty) {
            // TODO make all these asserts part of the [] operator
            Q_ASSERT(entities.contains(id));
            // TODO note that this is a bit awkward, since we already have the entity from above
            // Feel free to change this to result.entity = entity;
            result.entity = entities[id];
            result.geometryRendererId = entity->m_geometryRendererComponent;
        }

        if (!entity->isEnabled())
            return result;

        if (result.geometryRendererId.isNull())
            return result;

        // Geometry renderer is dirty if the entity is new, or if the geometry renderer itself changed
        // TODO repeated pattern - can we generalize this somehow?
        bool geometryRendererDirty = entityDirty || geometryRenderers.hasDirtyOrCreated(result.geometryRendererId);
        if (geometryRendererDirty) {
            Q_ASSERT(geometryRenderers.contains(result.geometryRendererId));
            const auto &geometryRenderer = geometryRenderers[result.geometryRendererId];
            result.geometryRenderer = geometryRenderer;
            result.geometryId = geometryRenderer->geometryId();
        }

        if (result.geometryId.isNull())
            return result;

        // TODO verify this
        if (result.geometryRenderer->primitiveType() == QGeometryRenderer::Patches)
            return result;

        // TODO what if we have a geometry factory?
        bool geometryDirty = geometryRendererDirty || geometries.hasDirtyOrCreated(result.geometryId);
        if (geometryDirty) {
            result.positionAttributeId = QNodeId(); // Reset the ID in case it was set previously
            result.indexAttributeId = QNodeId();
            Q_ASSERT(geometries.contains(result.geometryId));
            result.geometry = geometries[result.geometryId];

            // Use the default position attribute if attribute is null
            if (result.geometry->boundingPositionAttribute().isNull()) {
                const auto attrIds = result.geometry->attributes();
                for (const Qt3DCore::QNodeId &attrId : attrIds) {
                    Q_ASSERT(attributes.contains(attrId));
                    const auto &attribute = attributes[attrId];
                    if (attribute->name() == QAttribute::defaultPositionAttributeName()) {
                        result.positionAttributeId = attrId;
                        break;
                    }
                }
            } else {
                result.positionAttributeId = result.geometry->boundingPositionAttribute();
            }

            // Find index attribute Id
            const QVector<Qt3DCore::QNodeId> geometryAttributes = result.geometry->attributes();
            for (Qt3DCore::QNodeId attrNodeId : geometryAttributes) {
                Q_ASSERT(attributes.contains(attrNodeId));
                const auto &attribute = attributes[attrNodeId];
                if (attribute->attributeType() == Qt3DRender::QAttribute::IndexAttribute) {
                    result.indexAttributeId = attrNodeId;
                    break;
                }
            }
        }

        if (result.positionAttributeId.isNull()) {
            qWarning("WARNING: calculateLocalBoundingVolume: Position attribute not found. "
                     "Cannot perform bounding volume computation");
            return result;
        }

        bool positionAttributeDirty = geometryDirty || attributes.hasDirtyOrCreated(result.positionAttributeId);
        if (positionAttributeDirty) {
            Q_ASSERT(attributes.contains(result.positionAttributeId));
            result.positionAttribute = attributes[result.positionAttributeId];
            result.positionBufferId = result.positionAttribute->bufferId();
        }

        if (result.positionBufferId.isNull()) {
            qWarning("WARNING: calculateLocalBoundingVolume: Position attribute not referencing a valid buffer");
            return result;
        }

        if (result.positionAttribute->attributeType() != QAttribute::VertexAttribute
            || result.positionAttribute->vertexBaseType() != QAttribute::Float
            || result.positionAttribute->vertexSize() < 3) {
            qWarning("WARNING: calculateLocalBoundingVolume: Position attribute not suited for bounding volume computation");
            return result;
        }

        bool indexAttributeDirty = geometryDirty || attributes.hasDirtyOrCreated(result.indexAttributeId);
        if (indexAttributeDirty) {
            Q_ASSERT(attributes.contains(result.indexAttributeId));
            result.indexAttribute = attributes[result.indexAttributeId];
            result.indexBufferId = result.indexAttribute->bufferId();
        }

        if (result.indexAttribute->vertexBaseType() != QAttribute::UnsignedShort
            && result.indexAttribute->vertexBaseType() != QAttribute::UnsignedInt) {
            qWarning("WARNING: calculateLocalBoundingVolume: Unsupported index attribute type");
            return result;
        }

        int drawVertexCount = [result]() {
            if (result.geometryRenderer->vertexCount() > 0)
                return result.geometryRenderer->vertexCount();

            if (result.indexAttribute->count() > 0)
                return int(result.indexAttribute->count());
            return 0;
        }();

        bool indexBufferDirty = indexAttributeDirty || buffers.hasDirtyOrCreated(result.indexBufferId);
        if (indexBufferDirty) {
            result.indexBuffer = buffers[result.indexBufferId];
        }

        bool positionBufferDirty = positionAttributeDirty || buffers.hasDirtyOrCreated(result.positionBufferId);
        if (indexBufferDirty) {
            result.positionBuffer = buffers[result.positionBufferId];
        }

        BoundingVolumeCalculator reader;

        // TODO what about the case where we have no indexAttribute?
        if (positionAttributeDirty || positionBufferDirty || indexAttributeDirty
            || indexBufferDirty) {
            if (reader.apply(*result.positionAttribute, *result.positionBuffer, *result.indexAttribute, *result.indexBuffer, drawVertexCount,
                             result.geometryRenderer->primitiveRestartEnabled(), result.geometryRenderer->restartIndexValue())) {
                result.boundingVolume.setCenter(reader.result().center());
                result.boundingVolume.setRadius(reader.result().radius());
            }
        }

        return result;
    };

    localBoundingVolumes = rebuildAll(std::move(localBoundingVolumes),
                                      entities,
                                      calculateBoundingVolume);

    return localBoundingVolumes;
}

WorldBoundingVolumes calculateWorldBoundingVolumes(WorldBoundingVolumes worldBoundingVolumes,
                                                   const LocalBoundingVolumes &localBoundingVolumes,
                                                   const ValueContainer<Matrix4x4> &worldTransforms)
{
    worldBoundingVolumes.reset();

    if (!localBoundingVolumes.anythingDirty() && !worldTransforms.anythingDirty())
        return worldBoundingVolumes;

    auto calculateBoundingVolume = [=](const QNodeId &id,
                                       const Immutable<LocalBoundingVolumeResult> &localBoundingVolume) {
        BoundingVolume result;
            Q_ASSERT(worldTransforms.contains(id));
        result.local = localBoundingVolume->boundingVolume;
        result.world = result.local.transformed(*worldTransforms[id]);
        // TODO fix this
        result.worldWithChildren = result.world;

        return result;
    };

    worldBoundingVolumes = rebuildAll(std::move(worldBoundingVolumes),
                                      localBoundingVolumes,
                                      calculateBoundingVolume);

    return worldBoundingVolumes;
}
} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

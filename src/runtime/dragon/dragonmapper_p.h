/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONMAPPER_P_H
#define DRAGONMAPPER_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonbackendnode_p.h>
#include <private/dragonvaluecontainer_p.h>
#include <private/dragonnodetree_p.h>

#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertynoderemovedchange.h>
#include <Qt3DCore/qpropertyupdatedchange.h>

#include <Qt3DCore/qpropertyupdatedchange.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

template<typename T, typename U>
class MapperWrapper;

/*!
 * Simple wrapper which works around the fact that the frontend/arbiter expects a
 * pointer to a constant object, while we want the data to be stored in an immutable
 * or cow handle (the jury is still out on which to choose).
 * T should be derived from Dragon::BackendNode
 */
template <class Backend, class BackendTarget=Backend>
class NodeFunctor : public Qt3DCore::QBackendNodeMapper
{
public:
    NodeFunctor(NodeTree *nodeTree)
        : m_localNodes(new ValueContainer<Backend>())
        , m_nodes(m_localNodes.get())
        , m_nodeTree(nodeTree)
    {}

    NodeFunctor(NodeTree *nodeTree, ValueContainer<Backend> *nodes)
        : m_nodes(nodes)
        , m_nodeTree(nodeTree)
    {}

    // TODO constructors, one takes nothing, creates nodes - other takes existing list
    Qt3DCore::QBackendNode *create(const Qt3DCore::QNodeCreatedChangeBasePtr &change) const final;

    Qt3DCore::QBackendNode *get(Qt3DCore::QNodeId id) const final
    {
        return m_wrappers.value(id);
    }

    void destroy(Qt3DCore::QNodeId id) const final
    {
        m_nodeTree->removeNode(id);

        auto &nodes = *m_nodes;
        // TODO consider use modified to show there's a copy
        nodes.remove(id);
        delete m_wrappers.take(id);
    }

    void initializeFromPeer(Qt3DCore::QNodeId id, const Qt3DCore::QNodeCreatedChangeBasePtr &change) const
    {
        m_nodeTree->setParent(id, change->parentId());

        auto &nodes = *m_nodes;

        // Using a std::move here because we are assigning to the same node.
        nodes[id] = std::move(nodes[id]).modified([&change](Backend *node){
            node->initializeFromPeer(change);
            node->setEnabled(change->isNodeEnabled());
        });
    }

    void sceneChangeEvent(Qt3DCore::QNodeId id, const Qt3DCore::QSceneChangePtr &e) const
    {
        switch (e->type()) {
        case Qt3DCore::PropertyUpdated: {
            Qt3DCore::QPropertyUpdatedChangePtr change = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
            if (change->propertyName() == QByteArrayLiteral("parent")) {
                m_nodeTree->setParent(id, change->value().value<Qt3DCore::QNodeId>());
            }
            break;
        }
        default:
            break;
        }

        auto &nodes = *m_nodes;

        // Using a std::move here because we are assigning to the same node.
        nodes[id] = std::move(nodes[id]).modified([&e](Backend *element){
            if (e->type() == Qt3DCore::PropertyUpdated) {
                Qt3DCore::QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
                if (propertyChange->propertyName() == QByteArrayLiteral("enabled")) {
                    element->setEnabled(propertyChange->value().toBool());
                    element->markDirty();
                }
            }
            element->sceneChangeEvent(e);
        });

        // TODO consider adding a method to replace an item in the container directly:
        //    nodes.modify(id, [](Backend *element){
        //        element->sceneChangeEvent(e);
        //    });
    }

    // Task interface
    ValueContainer<Backend> result()
    {
        return *m_nodes;
    }

    bool isFinished() const
    {
        return true;
    }

    // TODO a bit annoying that we need this to be run at the end of the frame
    void reset()
    {
        m_nodes->reset();
    }

private:

    // NOTE mutable only because QBackendNodeMapper has a const interface (not sure why this is)
    // TODO consider changing QBackendNodeMapper for this reason, or add a pointer in-between as hacky
    // workaround
    mutable QHash<Qt3DCore::QNodeId, MapperWrapper<Backend, BackendTarget> *> m_wrappers;
    // TODO consider storing ValueContainer<BackendNode> (need to verify polymorphism)
    mutable QScopedPointer<ValueContainer<Backend>> m_localNodes;
    mutable ValueContainer<Backend> *m_nodes;
    mutable NodeTree *m_nodeTree = nullptr;
};

template <typename T, typename U>
class MapperWrapper : public Qt3DCore::QBackendNode
{
public:
    MapperWrapper(const NodeFunctor<T, U> *manager)
        : Qt3DCore::QBackendNode()
        , m_manager(manager)
    {
    }

protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override
    {
        m_manager->sceneChangeEvent(peerId(), e);
    }

private:
    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) override
    {
        m_manager->initializeFromPeer(peerId(), change);
    }

    const NodeFunctor<T, U> *m_manager = nullptr;
};

// Note: Implementation needs to be here because we need the definition of MapperWrapper
template<class Backend, class BackendTarget>
Qt3DCore::QBackendNode *NodeFunctor<Backend, BackendTarget>::create(const Qt3DCore::QNodeCreatedChangeBasePtr &change) const
{
    m_nodeTree->addNode(change->subjectId());

    auto peerId = change->subjectId();
    auto *wrapper = new MapperWrapper<Backend, BackendTarget>(this);
    m_wrappers[peerId] = wrapper;
    BackendTarget node;
    node.setPeerId(peerId);
    // TODO consider getting rid of this long-lived pointer
    node.setManager(m_nodes);
    m_nodes->insert(peerId, node);
    return wrapper;
}

template<typename T, typename U=T>
using NodeFunctorPtr = QSharedPointer<NodeFunctor<T, U>>;

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // DRAGONMAPPER_P_H

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONRENDERVIEW_P_H
#define DRAGONRENDERVIEW_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonattribute_p.h>
#include <private/dragonbuffer_p.h>
#include <private/dragoncameralens_p.h>
#include <private/dragongeometry_p.h>
#include <private/dragongeometryrenderer_p.h>
#include <private/dragonmaterial_p.h>
#include <private/dragonshader_p.h>
#include <private/dragonimmutable_p.h>
#include <private/dragonshaderparameterpack_p.h>
#include <private/dragonrenderstateset_p.h>
#include <private/dragonstatesetnode_p.h>
#include <private/dragonlayerfilternode_p.h>

#include <private/dragontechniquefilternode_p.h>
#include <private/dragonrenderpassfilternode_p.h>
#include <private/dragonrendertargetoutput_p.h>
#include <private/dragonblitframebuffer_p.h>
#include <private/dragonrendertarget_p.h>

#include <private/dragonattachment_p.h>

#include <private/dragonoptional_p.h>

#include <Qt3DRender/qclearbuffers.h>
#include <Qt3DRender/qmemorybarrier.h>
#include <Qt3DRender/qsortpolicy.h>
#include <Qt3DRender/qblitframebuffer.h>

// Borrowed from Qt3DRender::Render (they are fine, simple structs)
#include <Qt3DRender/private/uniform_p.h>

#include <QOpenGLContext>

QT_BEGIN_NAMESPACE
namespace Qt3DRender {
namespace Dragon {

class Parameter;
class Entity;
class RenderCommand;

struct ClearColorInfo
{
    int drawBufferIndex = 0;
    QRenderTargetOutput::AttachmentPoint attchmentPoint = QRenderTargetOutput::Color0;
    QVector4D clearColor;
};

struct ClearBackBufferInfo
{
    QClearBuffers::BufferTypeFlags buffers;
    float depth = 1.f;
    int stencil = 0;
    QVector4D color;
};

struct BlitFramebufferInfo
{
    Optional<Immutable<RenderTarget>> sourceRenderTarget;
    Optional<AttachmentPack> sourceAttachments;
    Optional<Immutable<RenderTarget>> destinationRenderTarget;
    Optional<AttachmentPack> destinationAttachments;
    QRect sourceRect;
    QRect destinationRect;
    Qt3DRender::QRenderTargetOutput::AttachmentPoint sourceAttachmentPoint;
    Qt3DRender::QRenderTargetOutput::AttachmentPoint destinationAttachmentPoint;
    QBlitFramebuffer::InterpolationMethod interpolationMethod;
    Immutable<BlitFramebuffer> node;
};

struct CameraMatrices {
    Matrix4x4 projectionMatrix;
    Matrix4x4 viewMatrix;
    Matrix4x4 viewProjectionMatrix;
    Vector3D eyePosition;
    Vector3D eyeViewDirection;
};

struct RenderView
{
    Qt3DCore::QNodeId leafNodeId;

    QSurface *surface = nullptr; // TODO consider wrapping this together with a surface locker
    bool hasCamera = false;
    QMemoryBarrier::Operations memoryBarrier{};

    // Render target
    Qt3DCore::QNodeId renderTargetId; // used only to see if has been already set, consider using Optional
    AttachmentPack attachmentPack;

    ClearBackBufferInfo clearBackBufferInfo;               // global ClearColor
    QHash<Qt3DCore::QNodeId, ClearColorInfo> specificClearColorBuffers;   // different draw buffers with distinct colors

    // TODO turn into a vector of filters
    Immutable<TechniqueFilter> techniqueFilter;
    bool hasTechniqueFilter = false;

    // TODO turn into a vector of filters
    Immutable<RenderPassFilter> renderPassFilter;
    bool hasRenderPassFilter = false;

    QVector<Immutable<LayerFilterNode>> layerFilters;

    // Viewport
    QRectF viewport = QRectF(0.f, 0.f, 1.f, 1.f);
    float gamma = 2.2f;
    QSize surfaceSize;
    float devicePixelRatio = 1.f;

    // Misc needed to set uniforms
    Immutable<CameraLens> cameraLens;
    Immutable<Entity> cameraNode;

    // Render state set
    Immutable<RenderStateSet> renderStateSet;

    bool noDraw = false;

    // Misc
    QVector<Qt3DRender::QSortPolicy::SortType> sortingTypes;
    Optional<BlitFramebufferInfo> blitFrameBufferInfo;

};
} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE

#endif // DRAGONRENDERVIEW_P_H

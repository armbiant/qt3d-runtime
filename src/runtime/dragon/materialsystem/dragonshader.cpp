/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonshader_p.h"

#include <Qt3DRender/private/qshaderprogram_p.h>

#include <QFile>
#include <QOpenGLContext>
#include <QOpenGLShaderProgram>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

Shader::Shader()
        : BackendNode()
        , m_dna(0)
        , m_oldDna(0)
        , m_status(QShaderProgram::NotReady)
{
    m_shaderCode.resize(static_cast<int>(QShaderProgram::Compute) + 1);
}

void Shader::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QShaderProgramData>>(change);
    const auto &data = typedChange->data;

    for (int i = QShaderProgram::Vertex; i <= QShaderProgram::Compute; ++i)
        m_shaderCode[i].clear();

    m_shaderCode[QShaderProgram::Vertex] = data.vertexShaderCode;
    m_shaderCode[QShaderProgram::TessellationControl] = data.tessellationControlShaderCode;
    m_shaderCode[QShaderProgram::TessellationEvaluation] = data.tessellationEvaluationShaderCode;
    m_shaderCode[QShaderProgram::Geometry] = data.geometryShaderCode;
    m_shaderCode[QShaderProgram::Fragment] = data.fragmentShaderCode;
    m_shaderCode[QShaderProgram::Compute] = data.computeShaderCode;
    updateDNA();
    //    markDirty(AbstractRenderer::ShadersDirty);
}

QVector<QString> Shader::uniformsNames() const
{
    return m_uniformsNames;
}

QVector<QString> Shader::attributesNames() const
{
    return m_attributesNames;
}

QVector<QString> Shader::uniformBlockNames() const
{
    return m_uniformBlockNames;
}

QVector<QString> Shader::storageBlockNames() const
{
    return m_shaderStorageBlockNames;
}

QVector<QByteArray> Shader::shaderCode() const
{
    return m_shaderCode;
}

void Shader::setShaderCode(QShaderProgram::ShaderType type, const QByteArray &code)
{
    if (code == m_shaderCode[type])
        return;

    m_shaderCode[type] = code;
    setStatus(QShaderProgram::NotReady);
    updateDNA();
    //    markDirty(AbstractRenderer::ShadersDirty);
}

void Shader::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    if (e->type() == PropertyUpdated) {
        QPropertyUpdatedChangePtr propertyChange = e.staticCast<QPropertyUpdatedChange>();
        QVariant propertyValue = propertyChange->value();

        if (propertyChange->propertyName() == QByteArrayLiteral("vertexShaderCode")) {
            setShaderCode(QShaderProgram::Vertex, propertyValue.toByteArray());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("fragmentShaderCode")) {
            setShaderCode(QShaderProgram::Fragment, propertyValue.toByteArray());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("tessellationControlShaderCode")) {
            setShaderCode(QShaderProgram::TessellationControl, propertyValue.toByteArray());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("tessellationEvaluationShaderCode")) {
            setShaderCode(QShaderProgram::TessellationEvaluation, propertyValue.toByteArray());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("geometryShaderCode")) {
            setShaderCode(QShaderProgram::Geometry, propertyValue.toByteArray());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("computeShaderCode")) {
            setShaderCode(QShaderProgram::Compute, propertyValue.toByteArray());
            markDirty();
        }
    }

    BackendNode::sceneChangeEvent(e);
}

QHash<QString, ShaderUniform> Shader::activeUniformsForUniformBlock(int blockIndex) const
{
    return m_uniformBlockIndexToShaderUniforms.value(blockIndex);
}

ShaderUniformBlock Shader::uniformBlockForBlockIndex(int blockIndex) const
{
    for (int i = 0, m = m_uniformBlocks.size(); i < m; ++i) {
        if (m_uniformBlocks[i].m_index == blockIndex) {
            return m_uniformBlocks[i];
        }
    }
    return ShaderUniformBlock();
}

ShaderUniformBlock Shader::uniformBlockForBlockNameId(int blockNameId) const
{
    for (int i = 0, m = m_uniformBlocks.size(); i < m; ++i) {
        if (m_uniformBlocks[i].m_nameId == blockNameId) {
            return m_uniformBlocks[i];
        }
    }
    return ShaderUniformBlock();
}

ShaderUniformBlock Shader::uniformBlockForBlockName(const QString &blockName) const
{
    for (int i = 0, m = m_uniformBlocks.size(); i < m; ++i) {
        if (m_uniformBlocks[i].m_name == blockName) {
            return m_uniformBlocks[i];
        }
    }
    return ShaderUniformBlock();
}

ShaderStorageBlock Shader::storageBlockForBlockIndex(int blockIndex) const
{
    for (int i = 0, m = m_shaderStorageBlockNames.size(); i < m; ++i) {
        if (m_shaderStorageBlocks[i].m_index == blockIndex)
            return m_shaderStorageBlocks[i];
    }
    return ShaderStorageBlock();
}

ShaderStorageBlock Shader::storageBlockForBlockNameId(int blockNameId) const
{
    for (int i = 0, m = m_shaderStorageBlockNames.size(); i < m; ++i) {
        if (m_shaderStorageBlocks[i].m_nameId == blockNameId)
            return m_shaderStorageBlocks[i];
    }
    return ShaderStorageBlock();
}

ShaderStorageBlock Shader::storageBlockForBlockName(const QString &blockName) const
{
    for (int i = 0, m = m_shaderStorageBlockNames.size(); i < m; ++i) {
        if (m_shaderStorageBlocks[i].m_name == blockName)
            return m_shaderStorageBlocks[i];
    }
    return ShaderStorageBlock();
}

// To be called from a worker thread
void Shader::submitPendingNotifications()
{
    //    const  QVector<Qt3DCore::QPropertyUpdatedChangePtr> notifications = std::move(m_pendingNotifications);
    //    for (const Qt3DCore::QPropertyUpdatedChangePtr &notification : notifications)
    //        notifyObservers(notification);
}

//void Shader::prepareUniforms(ShaderParameterPack &pack)
//{
//    const PackUniformHash &values = pack.uniforms();

//    auto it = values.cbegin();
//    const auto end = values.cend();
//    while (it != end) {
//        // Find if there's a uniform with the same name id
//        for (const ShaderUniform &uniform : qAsConst(m_uniforms)) {
//            if (uniform.m_nameId == it.key()) {
//                pack.setSubmissionUniform(uniform);
//                break;
//            }
//        }
//        ++it;
//    }
//}

//void Shader::setFragOutputs(const QHash<QString, int> &fragOutputs)
//{
//    {
//        QMutexLocker lock(&m_mutex);
//        m_fragOutputs = fragOutputs;
//    }
//    updateDNA();
//}

const QHash<QString, int> Shader::fragOutputs() const
{
    //    QMutexLocker lock(&m_mutex);
    return m_fragOutputs;
}

void Shader::updateDNA()
{
    m_oldDna = m_dna;
    uint codeHash = qHash(m_shaderCode[QShaderProgram::Vertex]
                          + m_shaderCode[QShaderProgram::TessellationControl]
                          + m_shaderCode[QShaderProgram::TessellationEvaluation]
                          + m_shaderCode[QShaderProgram::Geometry]
                          + m_shaderCode[QShaderProgram::Fragment]
                          + m_shaderCode[QShaderProgram::Compute]);

    //    QMutexLocker locker(&m_mutex);
    uint attachmentHash = 0;
    QHash<QString, int>::const_iterator it = m_fragOutputs.cbegin();
    QHash<QString, int>::const_iterator end = m_fragOutputs.cend();
    while (it != end) {
        attachmentHash += ::qHash(it.value()) + ::qHash(it.key());
        ++it;
    }
    const ProgramDNA newDNA = codeHash + attachmentHash;

    // Remove reference to shader based on DNA in the ShaderCache
    // In turn this will allow to purge the shader program if no other
    // Shader backend node references it
    // Note: the purge is actually happening occasionally in GraphicsContext::beginDrawing
    //    if (m_graphicsContext && newDNA != m_oldDna)
    //        m_graphicsContext->removeShaderProgramReference(this);

    m_dna = newDNA;
}

//void Shader::initializeUniforms(const QVector<ShaderUniform> &uniformsDescription)
//{
//    m_uniforms = uniformsDescription;
//    m_uniformsNames.resize(uniformsDescription.size());
//    m_uniformsNamesIds.resize(uniformsDescription.size());
//    QHash<QString, ShaderUniform> activeUniformsInDefaultBlock;

//    for (int i = 0, m = uniformsDescription.size(); i < m; i++) {
//        m_uniformsNames[i] = m_uniforms[i].m_name;
//        m_uniforms[i].m_nameId = StringToInt::lookupId(m_uniformsNames[i]);
//        m_uniformsNamesIds[i] = m_uniforms[i].m_nameId;
//        if (uniformsDescription[i].m_blockIndex == -1) { // Uniform is in default block
////            qCDebug(Shaders) << "Active Uniform in Default Block " << uniformsDescription[i].m_name << uniformsDescription[i].m_blockIndex;
//            activeUniformsInDefaultBlock.insert(uniformsDescription[i].m_name, uniformsDescription[i]);
//        }
//    }
//    m_uniformBlockIndexToShaderUniforms.insert(-1, activeUniformsInDefaultBlock);
//}

//void Shader::initializeAttributes(const QVector<ShaderAttribute> &attributesDescription)
//{
//    m_attributes = attributesDescription;
//    m_attributesNames.resize(attributesDescription.size());
//    m_attributeNamesIds.resize(attributesDescription.size());
//    for (int i = 0, m = attributesDescription.size(); i < m; i++) {
//        m_attributesNames[i] = attributesDescription[i].m_name;
//        m_attributes[i].m_nameId = StringToInt::lookupId(m_attributesNames[i]);
//        m_attributeNamesIds[i] = m_attributes[i].m_nameId;
//        qCDebug(Shaders) << "Active Attribute " << attributesDescription[i].m_name;
//    }
//}

//void Shader::initializeUniformBlocks(const QVector<ShaderUniformBlock> &uniformBlockDescription)
//{
//    m_uniformBlocks = uniformBlockDescription;
//    m_uniformBlockNames.resize(uniformBlockDescription.size());
//    m_uniformBlockNamesIds.resize(uniformBlockDescription.size());
//    for (int i = 0, m = uniformBlockDescription.size(); i < m; ++i) {
//        m_uniformBlockNames[i] = m_uniformBlocks[i].m_name;
//        m_uniformBlockNamesIds[i] = StringToInt::lookupId(m_uniformBlockNames[i]);
//        m_uniformBlocks[i].m_nameId = m_uniformBlockNamesIds[i];
//        qCDebug(Shaders) << "Initializing Uniform Block {" << m_uniformBlockNames[i] << "}";

//        // Find all active uniforms for the shader block
//        QVector<ShaderUniform>::const_iterator uniformsIt = m_uniforms.cbegin();
//        const QVector<ShaderUniform>::const_iterator uniformsEnd = m_uniforms.cend();

//        QVector<QString>::const_iterator uniformNamesIt = m_uniformsNames.cbegin();
//        const QVector<QString>::const_iterator uniformNamesEnd = m_attributesNames.cend();

//        QHash<QString, ShaderUniform> activeUniformsInBlock;

//        while (uniformsIt != uniformsEnd && uniformNamesIt != uniformNamesEnd) {
//            if (uniformsIt->m_blockIndex == uniformBlockDescription[i].m_index) {
//                QString uniformName = *uniformNamesIt;
//                if (!m_uniformBlockNames[i].isEmpty() && !uniformName.startsWith(m_uniformBlockNames[i]))
//                    uniformName = m_uniformBlockNames[i] + QLatin1Char('.') + *uniformNamesIt;
//                activeUniformsInBlock.insert(uniformName, *uniformsIt);
//                qCDebug(Shaders) << "Active Uniform Block " << uniformName << " in block " << m_uniformBlockNames[i] << " at index " << uniformsIt->m_blockIndex;
//            }
//            ++uniformsIt;
//            ++uniformNamesIt;
//        }
//        m_uniformBlockIndexToShaderUniforms.insert(uniformBlockDescription[i].m_index, activeUniformsInBlock);
//    }
//}

//void Shader::initializeShaderStorageBlocks(const QVector<ShaderStorageBlock> &shaderStorageBlockDescription)
//{
//    m_shaderStorageBlocks = shaderStorageBlockDescription;
//    m_shaderStorageBlockNames.resize(shaderStorageBlockDescription.size());
//    m_shaderStorageBlockNamesIds.resize(shaderStorageBlockDescription.size());

//    for (int i = 0, m = shaderStorageBlockDescription.size(); i < m; ++i) {
//        m_shaderStorageBlockNames[i] = m_shaderStorageBlocks[i].m_name;
//        m_shaderStorageBlockNamesIds[i] = Render::StringToInt::lookupId(m_shaderStorageBlockNames[i]);
//        m_shaderStorageBlocks[i].m_nameId =m_shaderStorageBlockNamesIds[i];
////        qCDebug(Shaders) << "Initializing Shader Storage Block {" << m_shaderStorageBlockNames[i] << "}";
//    }
//}

/*!
   \internal
   Initializes this Shader's state relating to attributes, global block uniforms and
   and named uniform blocks by copying these details from \a other.
*/
void Shader::initializeFromReference(const Shader &other)
{
    Q_ASSERT(m_dna == other.m_dna);
    m_uniformsNamesIds = other.m_uniformsNamesIds;
    m_uniformsNames = other.m_uniformsNames;
    m_uniforms = other.m_uniforms;
    m_attributesNames = other.m_attributesNames;
    m_attributeNamesIds = other.m_attributeNamesIds;
    m_attributes = other.m_attributes;
    m_uniformBlockNamesIds = other.m_uniformBlockNamesIds;
    m_uniformBlockNames = other.m_uniformBlockNames;
    m_uniformBlocks = other.m_uniformBlocks;
    m_uniformBlockIndexToShaderUniforms = other.m_uniformBlockIndexToShaderUniforms;
    m_fragOutputs = other.m_fragOutputs;
    m_shaderStorageBlockNamesIds = other.m_shaderStorageBlockNamesIds;
    m_shaderStorageBlockNames = other.m_shaderStorageBlockNames;
    m_shaderStorageBlocks = other.m_shaderStorageBlocks;
    setStatus(other.status());
    setLog(other.log());
}

void Shader::setLog(const QString &log)
{
    if (log != m_log) {
        m_log = log;
        Qt3DCore::QPropertyUpdatedChangePtr e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
        e->setDeliveryFlags(Qt3DCore::QSceneChange::DeliverToAll);
        e->setPropertyName("log");
        e->setValue(QVariant::fromValue(m_log));
        m_pendingNotifications.push_back(e);
    }
}

void Shader::setStatus(QShaderProgram::Status status)
{
    if (status != m_status) {
        m_status = status;
        Qt3DCore::QPropertyUpdatedChangePtr e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
        e->setDeliveryFlags(Qt3DCore::QSceneChange::DeliverToAll);
        e->setPropertyName("status");
        e->setValue(QVariant::fromValue(m_status));
        m_pendingNotifications.push_back(e);
    }
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

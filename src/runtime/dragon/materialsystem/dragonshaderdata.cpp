/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonshaderdata_p.h"

#include <Qt3DCore/qdynamicpropertyupdatedchange.h>
#include <Qt3DCore/qpropertyupdatedchange.h>

#include <Qt3DCore/private/vector3d_p.h>
#include <Qt3DCore/private/vector4d_p.h>

#include <private/qshaderdata_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

namespace {

const int qNodeIdTypeId = qMetaTypeId<Qt3DCore::QNodeId>();

}

void ShaderData::initializeFromPeer(const QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QShaderDataData>>(change);
    const QShaderDataData &data = typedChange->data;

    m_propertyReader = data.propertyReader;

    for (const QPair<QByteArray, QVariant> &entry : data.properties) {
        if (entry.first == QByteArrayLiteral("data") ||
                entry.first == QByteArrayLiteral("childNodes")) // We don't handle default Node properties
            continue;
        const QVariant &propertyValue = entry.second;
        const QString propertyName = QString::fromLatin1(entry.first);

        m_originalProperties.insert(propertyName, propertyValue);

        // We check if the property is a QNodeId or QVector<QNodeId> so that we can
        // check nested QShaderData for update
        if (propertyValue.userType() == qNodeIdTypeId) {
            m_nestedShaderDataProperties.insert(propertyName, propertyValue);
        } else if (propertyValue.userType() == QMetaType::QVariantList) {
            QVariantList list = propertyValue.value<QVariantList>();
            if (list.count() > 0 && list.at(0).userType() == qNodeIdTypeId)
                m_nestedShaderDataProperties.insert(propertyName, propertyValue);
        }
    }

    // We look for transformed properties once the complete hash of
    // originalProperties is available
    QHash<QString, QVariant>::iterator it = m_originalProperties.begin();
    const QHash<QString, QVariant>::iterator end = m_originalProperties.end();

    while (it != end) {
        if (it.value().type() == QVariant::Vector3D) {
            // if there is a matching QShaderData::TransformType propertyTransformed
            QVariant value = m_originalProperties.value(it.key() + QLatin1String("Transformed"));
            // if that's the case, we apply a space transformation to the property
            if (value.isValid() && value.type() == QVariant::Int)
                m_transformedProperties.insert(it.key(), static_cast<TransformType>(value.toInt()));
        }
        ++it;
    }
}


//ShaderData *ShaderData::lookupResource(NodeManagers *managers, QNodeId id)
//{
//    return managers->shaderDataManager()->lookupResource(id);
//}

//ShaderData *ShaderData::lookupResource(QNodeId id)
//{
//    return ShaderData::lookupResource(m_managers, id);
//}

// Call by cleanup job (single thread)
void ShaderData::clearUpdatedProperties()
{
    // DISABLED: Is only useful when building UBO from a ShaderData, which is disable since 5.7
    //    const QHash<QString, QVariant>::const_iterator end = m_nestedShaderDataProperties.end();
    //    QHash<QString, QVariant>::const_iterator it = m_nestedShaderDataProperties.begin();

    //    while (it != end) {
    //        if (it.value().userType() == QMetaType::QVariantList) {
    //            const auto values = it.value().value<QVariantList>();
    //            for (const QVariant &v : values) {
    //                ShaderData *nested = lookupResource(v.value<QNodeId>());
    //                if (nested != nullptr)
    //                    nested->clearUpdatedProperties();
    //            }
    //        } else {
    //            ShaderData *nested = lookupResource(it.value().value<QNodeId>());
    //            if (nested != nullptr)
    //                nested->clearUpdatedProperties();
    //        }
    //        ++it;
    //    }
}

QVariant ShaderData::getTransformedProperty(const QString &name, const Matrix4x4 &viewMatrix)
{
    // Note protecting m_worldMatrix at this point as we assume all world updates
    // have been performed when reaching this point
    auto it = m_transformedProperties.find(name);
    if (it != m_transformedProperties.end()) {
        const TransformType transformType = it.value();
        switch (transformType) {
        case ModelToEye:
            return QVariant::fromValue(viewMatrix * m_worldMatrix * Vector3D(m_originalProperties.value(name).value<QVector3D>()));
        case ModelToWorld:
            return QVariant::fromValue(m_worldMatrix * Vector3D(m_originalProperties.value(it.key()).value<QVector3D>()));
        case ModelToWorldDirection:
            return QVariant::fromValue(Vector3D(m_worldMatrix * Vector4D(m_originalProperties.value(it.key()).value<QVector3D>(), 0.0f)));
        case NoTransform:
            break;
        }
    }
    return QVariant();
}

// Called by FramePreparationJob or by RenderView when dealing with lights
void ShaderData::updateWorldTransform(const Matrix4x4 &worldMatrix)
{
    if (m_worldMatrix != worldMatrix) {
        m_worldMatrix = worldMatrix;
    }
}

/*!
  \internal
  Lookup if the current ShaderData or a nested ShaderData has updated properties.
  UpdateProperties contains either the value of the propertie of a QNodeId if it's another ShaderData.
  Transformed properties are updated for all of ShaderData that have ones at the point.

  \note This needs to be performed for every top level ShaderData every time it is used.
  As we don't know if the transformed properties use the same viewMatrix for all RenderViews.
 */

ShaderData::TransformType ShaderData::propertyTransformType(const QString &name) const
{
    return m_transformedProperties.value(name, TransformType::NoTransform);
}

void ShaderData::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    if (!m_propertyReader.isNull() && e->type() == PropertyUpdated) {
        QString propertyName;
        QVariant propertyValue;

        if (auto propertyChange = qSharedPointerDynamicCast<QPropertyUpdatedChange>(e)) {
            propertyName = QString::fromLatin1(propertyChange->propertyName());
            propertyValue =  m_propertyReader->readProperty(propertyChange->value());
        } else if (auto propertyChange = qSharedPointerDynamicCast<QDynamicPropertyUpdatedChange>(e)) {
            propertyName = QString::fromLatin1(propertyChange->propertyName());
            propertyValue = m_propertyReader->readProperty(propertyChange->value());
        } else {
            Q_UNREACHABLE();
        }

        // Note we aren't notified about nested QShaderData in this call
        // only scalar / vec properties
        m_originalProperties.insert(propertyName, propertyValue);
        markDirty();
    }

    BackendNode::sceneChangeEvent(e);
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_CLEARBUFFERS_P_H
#define QT3DRENDER_DRAGON_CLEARBUFFERS_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonframegraphnode_p.h>

#include <Qt3DRender/qclearbuffers.h>

#include <QVector4D>
#include <QColor>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

class FrameGraphManager;

class ClearBuffers : public FrameGraphNode
{
public:
    ClearBuffers();
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;
    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) final;

    QClearBuffers::BufferType type;
    // in the backend we store this as a QVector4D, as the clearBuffer
    // functions do support all float values (also those not clamped to [0,1])
    // (for non-clamped float or int buffer types)
    // we don't support this in the frontend yet, but let's keep our options for now
    // and avoid QColor->QVector4D conversion every frame.
    QVector4D clearColor;
    // some clear operations only accept clamped values
    QColor clearColorAsColor;
    float clearDepthValue;
    int clearStencilValue;
    Qt3DCore::QNodeId colorBufferId;
};

} // namespace Dragon

} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_CLEARBUFFERS_P_H

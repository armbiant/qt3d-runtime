SOURCES += \
    $$PWD/dragonblitframebuffer.cpp \
    $$PWD/dragonbuffercapture.cpp \
    $$PWD/dragoncameraselectornode.cpp \
    $$PWD/dragonclearbuffers.cpp \
    $$PWD/dragondispatchcompute.cpp \
    $$PWD/dragonframegraphnode.cpp \
    $$PWD/dragonfrustumculling.cpp \
    $$PWD/dragonlayerfilternode.cpp \
    $$PWD/dragonmemorybarrier.cpp \
    $$PWD/dragonnodraw.cpp \
    $$PWD/dragonproximityfilter.cpp \
    $$PWD/dragonrendercapture.cpp \
    $$PWD/dragonrenderpassfilternode.cpp \
    $$PWD/dragonrendersurfaceselector.cpp \
    $$PWD/dragonrendertargetselectornode.cpp \
    $$PWD/dragonsortpolicy.cpp \
    $$PWD/dragonstatesetnode.cpp \
    $$PWD/dragontechniquefilternode.cpp \
    $$PWD/dragonviewportnode.cpp

HEADERS += \
    $$PWD/dragonblitframebuffer_p.h \
    $$PWD/dragonblitframebuffer_p.h \
    $$PWD/dragonbuffercapture_p.h \
    $$PWD/dragoncameraselectornode_p.h \
    $$PWD/dragonclearbuffers_p.h \
    $$PWD/dragondispatchcompute_p.h \
    $$PWD/dragonframegraphnode_p.h \
    $$PWD/dragonfrustumculling_p.h \
    $$PWD/dragonlayerfilternode_p.h \
    $$PWD/dragonmemorybarrier_p.h \
    $$PWD/dragonnodraw_p.h \
    $$PWD/dragonproximityfilter_p.h \
    $$PWD/dragonrendercapture_p.h \
    $$PWD/dragonrenderpassfilternode_p.h \
    $$PWD/dragonrendersurfaceselector_p.h \
    $$PWD/dragonrendertargetselectornode_p.h \
    $$PWD/dragonsortpolicy_p.h \
    $$PWD/dragonstatesetnode_p.h \
    $$PWD/dragontechniquefilternode_p.h \
    $$PWD/dragonviewportnode_p.h

/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.4
import QtStudio3D 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: rootItem

    property bool isOpen: true
    property real isOpenState: isOpen

    width: 250
    height: parent.height
    x: parent.width - (isOpenState * width)
    opacity: 0.4 + isOpenState * 0.6

    Behavior on isOpenState {
        NumberAnimation {
            duration: 500
            easing.type: Easing.InOutQuad
        }
    }

    Item {
        width: 60
        height: 60
        anchors.right: parent.left
        Image {
            anchors.centerIn: parent
            source: "arrow.png"
            width: 16
            height: width
            rotation: isOpenState * 180
        }

        MouseArea {
            anchors.fill: parent
            anchors.margins: -20
            onClicked: {
                rootItem.isOpen = !rootItem.isOpen
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#404040"
        border.width: 1
        border.color: "#e0e0e0"
        opacity: 0.6
    }

    Column {
        width: parent.width - 32
        anchors.horizontalCenter: parent.horizontalCenter
        y: 16
        spacing: 16
        visible: isOpenState > 0
        Text {
            id: fpsCount
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#ffffff"
            text: " "
            Timer {
                running: true
                repeat: true
                interval: 2000
                onTriggered: {
                    fpsCount.text = "~" + (mainView.frameCount/2).toFixed(1) + " FPS";
                    mainView.frameCount = 0;
                }
            }
        }
        ComboBox {
            id: stereoModeComboBox
            property var modesModel: [
                "StereoModeMono",
                "StereoModeTopBottom",
                "StereoModeLeftRight",
                "StereoModeAnaglyphRedCyan",
                "StereoModeAnaglyphGreenMagenta"]
            width: parent.width
            model: modesModel
            currentIndex: 1
            onCurrentTextChanged: {
                viewSettings.stereoMode = stereoModeComboBox.currentText;
            }
        }
        Slider {
            id: eyeSeparationSlider
            width: parent.width
            from: 0
            to: 15.0
            value: mainView.eyeSeparation
            focusPolicy: Qt.NoFocus
            enabled: stereoModeComboBox.currentIndex != 0
            opacity: enabled ? 1.0: 0.2
            onValueChanged: {
                mainView.eyeSeparation = value;
            }
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                y: -8
                z: 10
                text: "EYE SEPARATION: " + mainView.eyeSeparation.toFixed(2)
                color: "#ffffff"
            }
        }
        Item {
            width: 1
            height: 1
        }
        Button {
            width: parent.width
            text: "RELOAD"
            onClicked: s3dpres.reload()
            focusPolicy: Qt.NoFocus
        }
        Button {
            width: parent.width
            text: "OPEN"
            onClicked: openDialog.open()
            focusPolicy: Qt.NoFocus
        }
        Button {
            width: parent.width
            text: "FULLSCREEN"
            onClicked: mainView.toggleFullscreen();
            focusPolicy: Qt.NoFocus
        }
        Item {
            width: 1
            height: 1
        }
        Button {
            id: profTogBtn
            width: parent.width
            text: "PROFILE UI"
            focusPolicy: Qt.NoFocus
            onClicked: s3dpres.profileUiVisible = !s3dpres.profileUiVisible
        }
        Slider {
            id: profScaleSlider
            width: parent.width
            from: 50
            to: 400
            value: 100
            focusPolicy: Qt.NoFocus
            enabled: s3dpres.profileUiVisible
            opacity: enabled ? 1.0: 0.2
            onValueChanged: s3dpres.profileUiScale = value/100
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                y: -8
                z: 10
                text: "PROFILE UI SCALE: " + s3dpres.profileUiScale.toFixed(2)
                color: "#ffffff"
            }
        }
    }
}
